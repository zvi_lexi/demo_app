package com.lexifone.app.lexifonedemo;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.widget.EditText;

/**
 * Created by Zvi on 8/17/2015.
 */
public class CustomEditText extends EditText {

    OnBackKeyPressedEventListener backKeyPressedEventListener;

    public CustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

//    public CustomEditText(Context context, AttributeSet attrs, int defStyleAttr) {
//        super(context, attrs, defStyleAttr);
//    }
//
//    public CustomEditText(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
//        super(context, attrs, defStyleAttr, defStyleRes);
//    }

    @Override
    public boolean onKeyPreIme(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK && backKeyPressedEventListener != null) {
            backKeyPressedEventListener.onEvent();
        }
        return super.dispatchKeyEvent(event);
    }

    public void setOnBackKeyPressedEventListener(OnBackKeyPressedEventListener eventListener) {
        backKeyPressedEventListener = eventListener;
    }
}
