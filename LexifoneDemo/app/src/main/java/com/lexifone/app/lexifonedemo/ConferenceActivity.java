package com.lexifone.app.lexifonedemo;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.lexifone.sdk.LexifoneManager;
import com.lexifone.sdk.LexifoneSetting;

import java.util.Map;


public class ConferenceActivity extends BaseActivity {
    private static boolean REGISTERED = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_face2_face);
            super.setTag("ConferenceActivity");
            ApplicationManager.setApplicatioMode(ApplicationManager.ApplicatioMode.CONFERENCE);
            PreferenceManager.setDefaultValues(this, R.xml.pref_conference, true);
            synchronized (this) {
                if (!REGISTERED) {
                    REGISTERED = true;
                    registerListeners();
                }
            }
        } catch (Exception e) {
            handleError("onCreate - exception: " + e.getLocalizedMessage(), e);
        }
    }

    @Override
    protected void onDestroy() {
        synchronized (this) {
            unregisterListeners();
            REGISTERED = false;
        }
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_conference, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void init() {
        super.init();
        btTalk_f2f_2Lang = null;
        btTalk_f2f_2.setVisibility(View.GONE);
        viewNativeLang_f2f_2.setVisibility(View.GONE);
        globalSessionId = sharedPreferences.getString(SettingsActivity.PREF_GLOBAL_SESSION_ID, null);
        displayName = sharedPreferences.getString(SettingsActivity.PREF_DISPLAY_NAME, null);
    }

    @Override
    protected LexifoneSetting loadLexifoneSetting() {
        LexifoneSetting setting = super.loadLexifoneSetting();
        setting.setGlobalSessionId(globalSessionId);
        setting.setSpeakerId(speakerId);
        setting.setDisplayName(displayName);
        return setting;
    }

    @Override
    protected void handleTranslationResult(Map<String, String> resultMap) {
        try {
            String origLang = resultMap.get(LexifoneManager.RESULT_ORIGINAL_LANGUAGE);
            String transcript = resultMap.get(origLang);
            if (transcript != null && !transcript.isEmpty()) {
                if (origLang.equals(nativeLanguage) && speakerId.equals(resultMap.get(LexifoneManager.RESULT_SPEKAER_ID))) {
                    inflateBubbleTranslationLeft("me", transcript, null, R.id.f2f_contentLayout, null);
                    EditNativeText.setText("");
                } else {
                    String speaker = resultMap.get(LexifoneManager.RESULT_SPEKAER_NAME);
                    speaker = speaker == null || speaker.isEmpty() ? "anonymous" : speaker;
                    String translation = resultMap.get(nativeLanguage);
                    if (translation != null && !translation.isEmpty()) {
                        inflateBubbleTranslationRight(speaker, transcript, translation, R.id.f2f_contentLayout, nativeLanguage);
                        if (playTranslation) {
                            lexifoneManager.textToSpeech(nativeLanguage, translation);
                        }
                    } else {
                        logWarn("handleTranslationResult - no translation found");
                    }
                }
            } else {
                logWarn("handleTranslationResult - no transcription found");
            }
        } catch (Exception e) {
            handleError("handleTranslationResult - exception:" + e.getLocalizedMessage(), e);
        }
    }
}
