package com.lexifone.app.lexifonedemo;

/**
 * Created by Zvi on 8/17/2015.
 */
public interface OnBackKeyPressedEventListener {
    void onEvent();
}
