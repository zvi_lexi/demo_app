package com.lexifone.app.lexifonedemo;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.graphics.Color;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.RingtonePreference;
import android.text.TextUtils;
import android.util.Log;
import android.widget.EditText;

import com.lexifone.sdk.LanguageDescriptor;
import com.lexifone.sdk.LexifoneManager;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * A {@link PreferenceActivity} that presents a set of application settings. On
 * handset devices, settings are presented as a single list. On tablets,
 * settings are split by category, with category headers shown to the left of
 * the list of settings.
 * <p/>
 * See <a href="http://developer.android.com/design/patterns/settings.html">
 * Android Design: Settings</a> for design guidelines and the <a
 * href="http://developer.android.com/guide/topics/ui/settings.html">Settings
 * API Guide</a> for more information on developing a Settings UI.
 */
public class SettingsActivity extends PreferenceActivity {
    private static String TAG = "SettingsActivity";
    /**
     * Determines whether to always show the simplified settings UI, where
     * settings are presented in a single list. When false, settings are shown
     * as a master/detail two-pane view on tablets. When true, a single pane is
     * shown on tablets.
     */
    private static final boolean ALWAYS_SIMPLE_PREFS = false;

//    static final String PREF_CONF_NATIVE_LANG_NAME = "conf_native_language_list";
    static final String PREF_APP_VERSION_NUMBER = "version_number";
    static final String PREF_F2F_NATIVE_LANG_NAME = "f2f_native_language_list";
    static final String PREF_CONF_NATIVE_LANG_NAME = "conf_native_language_list";
    static final String PREF_FOREIGN_LANG_NAME = "foreign_language_list";
    static final String PREF_PALY_TRANSLATION = "play_translation";
    static final String PREF_USER_NAME = "userName";
    static final String PREF_PASSWORD = "password";
    static final String PREF_LEXIFONE_ADRESS = "lexifoneAddress";
    static final String PREF_ENCODE_DATA = "encodeData";
    static final String PREF_CUSTOMER_ID = "customerId";
    static final String PREF_SAMPLE_RATE = "sampleRate";
    static final String PREF_GLOBAL_SESSION_ID = "globalSessionId";
    static final String PREF_DISPLAY_NAME = "displayName";
    static final String PREF_SPEAKER_ID = "speakerId";


    LexifoneManager lexifoneManager = LexifoneManager.getInstance();
    private static String nativeDialect, foreignDialect;
    private static boolean handleEvent = true;
    static String[] entries = null, entryValues = null;

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        handleEvent = false;
        try {
            setupSimplePreferencesScreen();
            if(entries == null){
                prepareLanguageArrays();
            }
            if (ApplicationManager.isFace2FaceMode()) {
                loadF2FLanguages();
            }else if(ApplicationManager.isConferenceMode()){
                loadConfLanguages();
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "error loading languages: " + e.getLocalizedMessage());
        }finally {
            handleEvent = true;
        }
    }

    private void prepareLanguageArrays(){
        entries = new String[BaseActivity.reverseLanguageMap.size()];
        entryValues = new String[BaseActivity.reverseLanguageMap.size()];
        BaseActivity.reverseLanguageMap.keySet().toArray(entries);
        BaseActivity.reverseLanguageMap.values().toArray(entryValues);
    }

    private void loadConfLanguages() {
        ListPreference lp = (ListPreference) findPreference(PREF_CONF_NATIVE_LANG_NAME);
        updatePreferenceEntries(lp, entries, entryValues);
    }

    private void loadF2FLanguages() {
        ListPreference lp = (ListPreference) findPreference(PREF_F2F_NATIVE_LANG_NAME);
        updatePreferenceEntries(lp, entries, entryValues);

        lp = (ListPreference) findPreference(PREF_FOREIGN_LANG_NAME);
        updatePreferenceEntries(lp, entries, entryValues);
    }

    private String getLanguageDisplayName(LanguageDescriptor languageDescriptor){
        String langName = languageDescriptor.getDisplayName();
        if(languageDescriptor.isBeta()){
            langName = langName + BaseActivity.BETA_SUFFIX;
        }
        return langName;
    }

    private void updatePreferenceEntries(ListPreference listPreference, String[] entries, String[] entryValues) {
        listPreference.setEntries(entries);
        listPreference.setEntryValues(entryValues);
    }

    private String getSelectedLang(ListPreference listPreference) {
        return PreferenceManager.getDefaultSharedPreferences(listPreference.getContext())
                .getString(listPreference.getKey(), "");
    }

    /**
     * Shows the simplified settings UI if the device configuration if the
     * device configuration dictates that a simplified, single-pane UI should be
     * shown.
     */
    private void setupSimplePreferencesScreen() {
        if (!isSimplePreferences(this)) {
            return;
        }

        // In the simplified UI, fragments are not used at all and we instead
        // use the older PreferenceActivity APIs.

        // Add 'general' preferences.
//        PreferenceCategory fakeHeader = new PreferenceCategory(this);
//        fakeHeader.setTitle(R.string.pref_header_general);
//        getPreferenceScreen().addPreference(fakeHeader);

        if(ApplicationManager.isConferenceMode()){
            addPreferencesFromResource(R.xml.pref_conference);
            ListPreference lp = (ListPreference) findPreference(PREF_CONF_NATIVE_LANG_NAME);
            bindPreferenceSummaryToValue(lp);
            lp.setSummary(getLanguageDisplayName(BaseActivity.languageMap.get(getSelectedLang(lp))));
        }else{
            addPreferencesFromResource(R.xml.pref_f2f);
            ListPreference lp = (ListPreference) findPreference(PREF_F2F_NATIVE_LANG_NAME);
            bindPreferenceSummaryToValue(lp);
            lp.setSummary(getLanguageDisplayName(BaseActivity.languageMap.get(getSelectedLang(lp))));
            lp = (ListPreference) findPreference(PREF_FOREIGN_LANG_NAME);
            bindPreferenceSummaryToValue(lp);
            lp.setSummary(getLanguageDisplayName(BaseActivity.languageMap.get(getSelectedLang(lp))));
        }

        loadConnectionPref(lexifoneManager.notConnected());

        addPreferencesFromResource(R.xml.pref_general);
        bindPreferenceSummaryToValue(findPreference(PREF_APP_VERSION_NUMBER));

//
//        // Add 'notifications' preferences, and a corresponding header.
//        PreferenceCategory fakeHeader = new PreferenceCategory(this);
//        fakeHeader.setTitle(R.string.pref_header_notifications);
//        getPreferenceScreen().addPreference(fakeHeader);
//        addPreferencesFromResource(R.xml.pref_notification);
//
//        // Add 'data and sync' preferences, and a corresponding header.
//        fakeHeader = new PreferenceCategory(this);
//        fakeHeader.setTitle(R.string.pref_header_data_sync);
//        getPreferenceScreen().addPreference(fakeHeader);
//        addPreferencesFromResource(R.xml.pref_data_sync);
//
        // Bind the summaries of EditText/List/Dialog/Ringtone preferences to
        // their values. When their values change, their summaries are updated
        // to reflect the new value, per the Android Design guidelines.
//        bindPreferenceSummaryToValue(findPreference("example_text"));
//        bindPreferenceSummaryToValue(findPreference("example_list"));
//        bindPreferenceSummaryToValue(findPreference("notifications_new_message_ringtone"));
//        bindPreferenceSummaryToValue(findPreference("sync_frequency"));
    }

    private void loadConnectionPref(boolean enabled){
//        if(!enabled){
//            getWindow().getDecorView().setBackgroundColor(Color.GRAY);
//        }
        if(ApplicationManager.isConferenceMode()){
            addPreferencesFromResource(R.xml.pref_conference_connection);

            EditTextPreference globalSessionId = (EditTextPreference) findPreference(PREF_GLOBAL_SESSION_ID);
            globalSessionId.setEnabled(enabled);
            globalSessionId.setSummary(globalSessionId.getText());
            globalSessionId.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    final EditTextPreference pref = (EditTextPreference) findPreference(PREF_GLOBAL_SESSION_ID);
                    pref.setSummary(newValue.toString());
                    return true;
                }
            });

            // Register the EditTextPreference
            EditTextPreference displayName = (EditTextPreference) findPreference(PREF_DISPLAY_NAME);
            displayName.setEnabled(enabled);
            displayName.setSummary(displayName.getText());
            displayName.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    final EditTextPreference pref = (EditTextPreference) findPreference(PREF_DISPLAY_NAME);
                    pref.setSummary(newValue.toString());
                    return true;
                }
            });
        }

        addPreferencesFromResource(R.xml.pref_connection);
        // Register the EditTextPreference
        EditTextPreference username = (EditTextPreference) findPreference(PREF_USER_NAME);
        username.setEnabled(enabled);
        username.setSummary(username.getText());
        username.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                final EditTextPreference pref = (EditTextPreference) findPreference(PREF_USER_NAME);
                pref.setSummary(newValue.toString());
                return true;
            }
        });

        // Register the EditTextPreference
        EditTextPreference password = (EditTextPreference) findPreference(PREF_PASSWORD);
        password.setEnabled(enabled);
        String currentPassword = password.getText();
        password.setSummary(currentPassword);
        if (currentPassword != null) {
            EditText edit = password.getEditText();
            String maskedCurrentPassword = edit.getTransformationMethod().getTransformation(currentPassword.toString(), edit).toString();
            password.setSummary(maskedCurrentPassword);
        }
        password.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                final EditTextPreference password = (EditTextPreference) findPreference(PREF_PASSWORD);
                password.setSummary(newValue.toString());
                EditText edit = password.getEditText();
                String maskedNewPassword = edit.getTransformationMethod().getTransformation(newValue.toString(), edit).toString();
                password.setSummary(maskedNewPassword);
                return true;
            }
        });

        // Register the EditTextPreference
        EditTextPreference lexifoneAddressPref = (EditTextPreference) findPreference(PREF_LEXIFONE_ADRESS);
        lexifoneAddressPref.setEnabled(enabled);
        lexifoneAddressPref.setSummary(lexifoneAddressPref.getText());
        lexifoneAddressPref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                final EditTextPreference pref = (EditTextPreference) findPreference(PREF_LEXIFONE_ADRESS);
                pref.setSummary(newValue.toString());
                return true;
            }
        });

        CheckBoxPreference encodedPref = (CheckBoxPreference) findPreference(PREF_ENCODE_DATA);
        encodedPref.setEnabled(false);

        // Register the EditTextPreference
        EditTextPreference customerId = (EditTextPreference) findPreference(PREF_CUSTOMER_ID);
        customerId.setEnabled(enabled);
        customerId.setSummary(customerId.getText());
        customerId.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                final EditTextPreference pref = (EditTextPreference) findPreference(PREF_CUSTOMER_ID);
                pref.setSummary(newValue.toString());
                return true;
            }
        });

        // Register the ListPreference
        ListPreference sampleRate = (ListPreference) findPreference(PREF_SAMPLE_RATE);
        sampleRate.setEnabled(enabled);
        sampleRate.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                int i = ((ListPreference) preference).findIndexOfValue(newValue.toString());
                CharSequence[] entries = ((ListPreference) preference).getEntries();
                preference.setSummary(entries[i]);
                return true;
            }
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean onIsMultiPane() {
        return isXLargeTablet(this) && !isSimplePreferences(this);
    }

    /**
     * Helper method to determine if the device has an extra-large screen. For
     * example, 10" tablets are extra-large.
     */
    private static boolean isXLargeTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_XLARGE;
    }

    /**
     * Determines whether the simplified settings UI should be shown. This is
     * true if this is forced via {@link #ALWAYS_SIMPLE_PREFS}, or the device
     * doesn't have newer APIs like {@link PreferenceFragment}, or the device
     * doesn't have an extra-large screen. In these cases, a single-pane
     * "simplified" settings UI should be shown.
     */
    private static boolean isSimplePreferences(Context context) {
        return ALWAYS_SIMPLE_PREFS
                || Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB
                || !isXLargeTablet(context);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void onBuildHeaders(List<Header> target) {
        if (!isSimplePreferences(this)) {
            loadHeadersFromResource(R.xml.pref_headers, target);
        }
    }

    /**
     * A preference value change listener that updates the preference's summary
     * to reflect its new value.
     */
    private static Preference.OnPreferenceChangeListener sBindPreferenceSummaryToValueListener = new Preference.OnPreferenceChangeListener() {
        @Override
        public boolean onPreferenceChange(Preference preference, Object value) {
            String stringValue = value.toString();

            if (preference instanceof ListPreference) {
                // For list preferences, look up the correct display value in
                // the preference's 'entries' list.
                ListPreference listPreference = (ListPreference) preference;

                // if language changed - do validation
                boolean doValidate = false;
                String nativeDialect = SettingsActivity.nativeDialect;
                String foreignDialect = SettingsActivity.foreignDialect;
                if (PREF_F2F_NATIVE_LANG_NAME.equals(listPreference.getKey())) {
                    doValidate = true;
                    nativeDialect = stringValue;
                } else if (PREF_FOREIGN_LANG_NAME.equals(preference.getKey())) {
                    doValidate = true;
                    foreignDialect = stringValue;
                }
                doValidate &= ApplicationManager.isFace2FaceMode();
                if (SettingsActivity.handleEvent && doValidate && SettingsActivity.nativeDialect != null && SettingsActivity.foreignDialect != null) {
                    // validate that selected dialect are not of the same language
                    String validationResult = LexifoneManager.getInstance().validateLanguages(nativeDialect, foreignDialect);
                    if (validationResult != null) {
                        new AlertDialog.Builder(preference.getContext())
                                .setTitle("Language selection")
                                .setMessage("Selected languages are not valid: " + validationResult)
                                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                    }
                                }).setIcon(android.R.drawable.ic_dialog_alert).show();
                        return false;
                    }
                }

                // update static members
                SettingsActivity.nativeDialect = nativeDialect;
                SettingsActivity.foreignDialect = foreignDialect;

                // Set the summary to reflect the new value.
                int index = listPreference.findIndexOfValue(stringValue);
                preference.setSummary(index >= 0 ? listPreference.getEntries()[index] : null);
            } else if (preference instanceof RingtonePreference) {
                // For ringtone preferences, look up the correct display value
                // using RingtoneManager.
                if (TextUtils.isEmpty(stringValue)) {
                    // Empty values correspond to 'silent' (no ringtone).
                    preference.setSummary(R.string.pref_ringtone_silent);
                } else {
                    Ringtone ringtone = RingtoneManager.getRingtone(
                            preference.getContext(), Uri.parse(stringValue));
                    if (ringtone == null) {
                        // Clear the summary if there was a lookup error.
                        preference.setSummary(null);
                    } else {
                        // Set the summary to reflect the new ringtone display
                        // name.
                        String name = ringtone.getTitle(preference.getContext());
                        preference.setSummary(name);
                    }
                }

            } else {
                // For all other preferences, set the summary to the value's
                // simple string representation.
                preference.setSummary(stringValue);
            }
            return true;
        }
    };

    /**
     * Binds a preference's summary to its value. More specifically, when the
     * preference's value is changed, its summary (line of text below the
     * preference title) is updated to reflect the value. The summary is also
     * immediately updated upon calling this method. The exact display format is
     * dependent on the type of preference.
     *
     * @see #sBindPreferenceSummaryToValueListener
     */
    private static void bindPreferenceSummaryToValue(Preference preference) {
        // Set the listener to watch for value changes.
        preference.setOnPreferenceChangeListener(sBindPreferenceSummaryToValueListener);

        // Trigger the listener immediately with the preference's
        // current value.
        sBindPreferenceSummaryToValueListener.onPreferenceChange(preference,
                PreferenceManager
                        .getDefaultSharedPreferences(preference.getContext())
                        .getString(preference.getKey(), ""));
    }

    /**
     * This fragment shows general preferences only. It is used when the
     * activity is showing a two-pane settings UI.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class GeneralPreferenceFragment extends PreferenceFragment {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_general);

            // Bind the summaries of EditText/List/Dialog/Ringtone preferences
            // to their values. When their values change, their summaries are
            // updated to reflect the new value, per the Android Design
            // guidelines.
            bindPreferenceSummaryToValue(findPreference("example_text"));
            bindPreferenceSummaryToValue(findPreference("example_list"));
        }
    }

    /**
     * This fragment shows notification preferences only. It is used when the
     * activity is showing a two-pane settings UI.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class NotificationPreferenceFragment extends PreferenceFragment {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_notification);

            // Bind the summaries of EditText/List/Dialog/Ringtone preferences
            // to their values. When their values change, their summaries are
            // updated to reflect the new value, per the Android Design
            // guidelines.
            bindPreferenceSummaryToValue(findPreference("notifications_new_message_ringtone"));
        }
    }

    /**
     * This fragment shows data and sync preferences only. It is used when the
     * activity is showing a two-pane settings UI.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class DataSyncPreferenceFragment extends PreferenceFragment {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_data_sync);

            // Bind the summaries of EditText/List/Dialog/Ringtone preferences
            // to their values. When their values change, their summaries are
            // updated to reflect the new value, per the Android Design
            // guidelines.
            bindPreferenceSummaryToValue(findPreference("sync_frequency"));
        }
    }


}
