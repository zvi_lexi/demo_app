package com.lexifone.app.lexifonedemo;

/**
 * Created by Zvi on 8/16/2015.
 */
public class ApplicationManager {

    public enum ApplicatioMode {
        FACE_2_FACE, CONFERENCE;

        public boolean isFace2Face() {
            return this.ordinal() == FACE_2_FACE.ordinal();
        }

        public boolean isConference() {
            return this.ordinal() == CONFERENCE.ordinal();
        }
    }


    private static ApplicatioMode applicatioMode;

    static String nativeLanguage;

    public static ApplicatioMode getApplicatioMode() {
        return applicatioMode;
    }

    public static void setApplicatioMode(ApplicatioMode applicatioMode) {
        ApplicationManager.applicatioMode = applicatioMode;
    }

    public static boolean isConferenceMode() {
        return applicatioMode.isConference();
    }

    public static boolean isFace2FaceMode() {
        return applicatioMode.isFace2Face();
    }
}
