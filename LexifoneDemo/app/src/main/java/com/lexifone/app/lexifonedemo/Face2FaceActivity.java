package com.lexifone.app.lexifonedemo;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;

import com.lexifone.sdk.LexifoneManager;
import com.lexifone.sdk.LexifoneSetting;

import java.util.Map;


public class Face2FaceActivity extends BaseActivity {
    private static boolean REGISTERED = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_face2_face);
            super.setTag("Face2FaceActivity");
            ApplicationManager.setApplicatioMode(ApplicationManager.ApplicatioMode.FACE_2_FACE);
            PreferenceManager.setDefaultValues(this, R.xml.pref_f2f, true);
            synchronized (this) {
                if (!REGISTERED) {
                    REGISTERED = true;
                    registerListeners();
                }
            }
        } catch (Exception e) {
            handleError("onCreate - exception: " + e.getLocalizedMessage(), e);
        }
    }

    @Override
    protected void onDestroy() {
        synchronized (this) {
            unregisterListeners();
            REGISTERED = false;
        }
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_face2_face, menu);
        return true;
    }

//    @Override
//    protected void childToggleActionButtons(boolean enable) {
//        btTalk_f2f_1.setEnabled(enable);
//        btTalk_f2f_2.setEnabled(enable);
//    }

    @Override
    protected LexifoneSetting loadLexifoneSetting() {
        LexifoneSetting setting = super.loadLexifoneSetting();
        setting.setForeignLanguages(new String[]{foreignLanguage});
        return setting;
    }

    @Override
    protected void handleTranslationResult(Map<String, String> resultMap) {
        String origLang = resultMap.get(LexifoneManager.RESULT_ORIGINAL_LANGUAGE);
        if (origLang == null || origLang.isEmpty()) {
            Log.e("handleTranslationResult", "failed to extract original language from result");
            return;
        }
        String transcript, translation, translationLang;
        if (origLang.equals(btTalk_f2f_1Lang)) {
            transcript = resultMap.get(btTalk_f2f_1Lang);
            translation = resultMap.get(btTalk_f2f_2Lang);
            translationLang = btTalk_f2f_2Lang;
            inflateBubbleTranslationLeft(null, transcript, translation, R.id.f2f_contentLayout, translationLang);
            // *********left side
        } else {
            transcript = resultMap.get(btTalk_f2f_2Lang);
            translation = resultMap.get(btTalk_f2f_1Lang);
            translationLang = btTalk_f2f_1Lang;
            inflateBubbleTranslationRight(null, transcript, translation, R.id.f2f_contentLayout, translationLang);
            // *********right side
        }

        EditNativeText.setText("");
        if (playTranslation) {
            try {
                lexifoneManager.textToSpeech(translationLang, translation);
            } catch (Exception e) {
                handleError("handleTranslationResult - error playing translation audio", e);
            }
        }
    }

    @Override
    protected void init() {
        super.init();
        validateLanguages(nativeLanguage, foreignLanguage);
    }
}
