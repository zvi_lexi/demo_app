package com.lexifone.app.lexifonedemo;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.location.Location;
import android.location.LocationManager;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.lexifone.sdk.LanguageDescriptor;
import com.lexifone.sdk.LexifoneManager;
import com.lexifone.sdk.LexifoneSetting;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;
import java.util.UUID;

import io.fabric.sdk.android.Fabric;


public class BaseActivity extends ActionBarActivity implements View.OnTouchListener {
    private static String TAG = "BaseActivity";
    static SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
    static final int STOP_CAPTURE_WAIT = 1000;
    static final int DEFAULT_CONNECTION_TIMEOUT_SECONDS = 30;
    public static final String NEW_LINE = System.getProperty("line.separator");
    public static final String BETA_SUFFIX = " (Beta)";
//    private static boolean inBackground = false;
    protected String packageName;
    static String versionName;
    static int versionCode = -1;

    protected static LexifoneManager lexifoneManager = LexifoneManager.getInstance();

    public static final int AUDIO_SOURCE = MediaRecorder.AudioSource.MIC;
    public static final int CHANNEL_IN_CONFIG = AudioFormat.CHANNEL_IN_MONO;
    public static final int AUDIO_FORMAT = AudioFormat.ENCODING_PCM_16BIT;

    final int DEFAULT_SAMPLE_RATE = 8000;
    final int TEST_CONF = AudioFormat.CHANNEL_OUT_MONO;
    final int TEST_FORMAT = AudioFormat.ENCODING_PCM_16BIT;
    final int TEST_MODE = AudioTrack.MODE_STREAM;
    final int TEST_STREAM_TYPE = AudioManager.STREAM_MUSIC;

    String nativeLanguage, foreignLanguage;
    String userName;
    String password;
    String lexifoneAddress;
    boolean encodeData, playTranslation;
    String customerId;
    int sampleRate;
    String globalSessionId;
    String displayName;
    double longitude = 0;
    double latitude = 0;
    String speakerId = null;

    protected boolean captureAudio, audioReadingFinished = true, returnFromSetting;

    TextView viewNativeLang_f2f_1, viewNativeLang_f2f_2, viewChangedLang;
    CustomEditText EditNativeText;
    Switch switchStartStop;
    ImageButton btTalk_f2f_1, btTalk_f2f_2;

    ProgressBar progressBar = null;
    String btTalk_f2f_1Lang, btTalk_f2f_2Lang;
    boolean updateTalkButtons = false;
    static Map<String, LanguageDescriptor> languageMap = null;
    static Map<String, String> reverseLanguageMap = null;
    StringBuilder resultBuilder = new StringBuilder();
    static protected String[] languageDescriptionArray;


    SharedPreferences sharedPreferences;
    private BroadcastReceiver mSystemMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            handleSystemMessage(intent);
        }
    };

    private BroadcastReceiver mTranslationMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            handleTranslateResult(intent);
        }
    };

    private BroadcastReceiver mAudioMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            byte[] data = intent.getByteArrayExtra(lexifoneManager.MSG_KEY);
//            int sampleRate = intent.getIntExtra(lexifoneManager.SAMPLE_RATE_KEY, DEFAULT_SAMPLE_RATE);
            playMedia(data, DEFAULT_SAMPLE_RATE);
        }
    };

    private BroadcastReceiver mStatusMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Log.d("receiver", "Got action: " + action);
            if (lexifoneManager.EVENT_CONNECT_FILTER.equals(action)) {
                handleConnectStatusMsg(intent);
            } else if (lexifoneManager.EVENT_CHANGE_LANGS_FILTER.equals(action)) {
                handleChangeLanguagesResult(intent);
            } else {
                Log.e("receiver", "unsupported action: " + action);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_base);
    }

    private void writeToLog(String sFileName, String sBody, Exception e) {
        try {
            Log.e(TAG, sBody);
            if (e != null) {
                StringBuilder sb = new StringBuilder();
                for (StackTraceElement element : e.getStackTrace()) {
                    sb.append(element.toString() + NEW_LINE);
                }
                Log.e(TAG, sb.toString());
            }

//            String s = Build.HARDWARE;
            boolean isEmulator = "goldfish".equals(Build.HARDWARE);
            if (isEmulator) {
                String permission = "android.permission.WRITE_EXTERNAL_STORAGE";
                int res = this.checkCallingOrSelfPermission(permission);
                if (res != PackageManager.PERMISSION_GRANTED) {
                    Log.w(TAG, "writeToLog - write permission not granted");
                    return;
                } else {
                    Log.w(TAG, "writeToLog - write permission granted");
                }
            }
            File root = new File(Environment.getExternalStorageDirectory(), "Notes");
            if (!root.exists()) {
                root.mkdirs();
            }
            File gpxfile = new File(root, sFileName);
            FileWriter writer = new FileWriter(gpxfile);
            writer.append(sBody);
            writer.flush();
            writer.close();
//            Toast.makeText(this, "Done", Toast.LENGTH_SHORT).show();
        } catch (Exception e1) {
            e1.printStackTrace();

        }
    }

//    private void writeToLog(String message){
//        if(logWriter == null)
//        try {
//            logWriter.write(message);
//            logWriter.write(NEW_LINE);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }

    static String timeStamp() {
        return " - " + dateFormat.format(new Date());
    }

    @Override
    protected void onResume() {
        super.onResume();
        viewChangedLang = null;
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        if(versionCode < 0){
            versionCode = BuildConfig.VERSION_CODE;
            versionName = BuildConfig.VERSION_NAME;
            if(versionCode >= 0 && versionName != null && !versionName.isEmpty()) {
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString(SettingsActivity.PREF_APP_VERSION_NUMBER, versionName + "." + versionCode);
                editor.commit();
            }
        }

//        if (inBackground) {
//            inBackground = false;
//        }
        updateTalkButtons = false;
        try {
            if(lexifoneManager.connectionOpen()) {
                if (returnFromSetting) {
                    updateTalkButtons = true;
                    updateSettings();
                    sendChangeLanguageRequest();
                }
            }else {
                if (returnFromSetting) {
                    ApplicationManager.nativeLanguage = sharedPreferences.getString(getNativeLanPrefKey(), null);
                }
                init();
            }
        } catch (Exception e) {
            writeToLog("initException.log", e.getLocalizedMessage(), e);
        }
        returnFromSetting = false;
    }

//    @Override
//    protected void onUserLeaveHint() {
//        inBackground = true;
//        if (lexifoneManager != null && lexifoneManager.getConnectStatus().connectionOpen()) {
//            try {
//                lexifoneManager.stop();
//            } catch (Exception e) {
//                Log.e(TAG, "failed to stop lexifoneManager");
//            }
//        }
//        super.onUserLeaveHint();
//    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            returnFromSetting = true;
            startActivity(new Intent(BaseActivity.this, SettingsActivity.class));
            return true;
        }

        if (id == R.id.action_conference) {
            if (lexifoneManager.notConnected()) {
                changeMode(this, ConferenceActivity.class);
            } else {
                displayToast(R.string.lexifone_change_modenotavailable);
            }
            return true;
        }
        if (id == R.id.action_face_to_face) {
            if (lexifoneManager.notConnected()) {
                changeMode(this, Face2FaceActivity.class);
            } else {
                displayToast(R.string.lexifone_change_modenotavailable);
            }
            return true;
        }
        if (id == R.id.clean) {
            ViewGroup content = (ViewGroup) findViewById(R.id.f2f_contentLayout);
            cleanDialog(R.string.cleanMassage, R.string.cleanTitle, content);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private Boolean exit = false;

    @Override
    public void onBackPressed() {
        if (exit) {
            finish(); // finish activity
        } else {
            displayToast("Press Back again to Exit.", Toast.LENGTH_SHORT);
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 3 * 1000);
        }
    }

    @Override
    protected void onDestroy() {
        if (lexifoneManager != null && lexifoneManager.connectionOpen()) {
            try {
                lexifoneManager.stop();
            } catch (Exception e) {
                Log.e(TAG, "failed to stop lexifoneManager");
            }
        }
        super.onDestroy();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public synchronized boolean onTouch(View v, MotionEvent event) {
        try {
            final ImageButton bt_1 = (ImageButton) findViewById(R.id.f2f_talk_bt_1);
            final ImageButton bt_2 = (ImageButton) findViewById(R.id.f2f_talk_bt_2);

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    if (v.getId() == R.id.f2f_talk_bt_1) {
                        if (captureAudio) {
                            return true;
                        }
                        v.setBackgroundResource(R.drawable.audio_button_down);
                        bt_2.setClickable(false);

                        try {
                            if (ApplicationManager.isFace2FaceMode() && btTalk_f2f_1Lang.equals(foreignLanguage)) {
                                updateTalkButtons = false;
                                sendChangeLanguageRequest(foreignLanguage, ApplicationManager.isConferenceMode() ? null : nativeLanguage);
                            }
                            startCaptureAudio();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    if (v.getId() == R.id.f2f_talk_bt_2) {
                        if (captureAudio) {
                            return true;
                        }
                        v.setBackgroundResource(R.drawable.audio_button_down);
                        bt_1.setClickable(false);

                        try {
                            if (ApplicationManager.isFace2FaceMode() && btTalk_f2f_2Lang.equals(foreignLanguage)) {
                                updateTalkButtons = false;
                                sendChangeLanguageRequest(foreignLanguage, nativeLanguage);
                            }
                            startCaptureAudio();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case MotionEvent.ACTION_UP:
                    if (v.getId() == R.id.f2f_talk_bt_1) {
                        try {
                            stopCaptureAudio();
                        }catch (Exception e){
                            throw e;
                        }finally {
                            v.setBackgroundResource(R.drawable.audio_button_normal);
                            bt_2.setClickable(true);
                        }
                    }
                    if (v.getId() == R.id.f2f_talk_bt_2) {
                        try {
                            stopCaptureAudio();
                        }catch (Exception e){
                            throw e;
                        }finally {
                            v.setBackgroundResource(R.drawable.audio_button_normal_2);
                            bt_1.setClickable(true);
                        }
                    }
                    break;
            }
        }catch (Exception e){
            handleError("Error in onTouch", e);
        }
        return true;
    }

    public void base_translateText(View view, String nativeLanguage, String[] foreignLanguages) {
        String textToTranslate = EditNativeText.getText().toString();
        if (textToTranslate.length() > 0) {
            try {
                lexifoneManager.translateText(textToTranslate, nativeLanguage, foreignLanguages);
                InputMethodManager inputManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

//                displayToast(getString(R.string.performing_translation));
            } catch (Exception e) {
                handleError(e);
            }
        }
    }

    public void makeLanguageDialog(View v) {
        ListAdapter languageList = new ArrayAdapter<String>(this, R.layout.array_adapter_list_view, languageDescriptionArray);//abc_action_menu_item_layout
        AlertDialog.Builder languageMenu = new AlertDialog.Builder(this);
        final int langButtonId = v.getId();

        // setting dialog
        languageMenu.setTitle("choose language:");
        languageMenu.setIcon(R.drawable.change_language_icon);
        languageMenu.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                return;
            }
        });

        languageMenu.setAdapter(languageList, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                String chooseLang = languageDescriptionArray[which];
                String langCode = reverseLanguageMap.get(chooseLang);

                if (langCode != null) {
                    updateTalkButtons = true;
                    boolean connected = lexifoneManager.userValidated();
                    if (langButtonId == R.id.f2f_native_lang_title_tv_1 || langButtonId == R.id.f2f_native_lang_img_1) { //left
                        viewChangedLang = viewNativeLang_f2f_1;
                        if (btTalk_f2f_1Lang.equals(nativeLanguage)) {
                            if (connected) {
                                sendChangeLanguageRequest(langCode, btTalk_f2f_2Lang);
                            } else {
                                if(ApplicationManager.isConferenceMode() || validateLanguages(langCode, foreignLanguage)) {
                                    nativeLanguage = btTalk_f2f_1Lang = langCode;
                                    commitNativeLangLanguage(viewNativeLang_f2f_1, R.id.f2f_native_lang_img_1);
                                }
                            }
                        } else {
                            if (connected) {
                                sendChangeLanguageRequest(btTalk_f2f_2Lang, langCode);
                            } else {
                                if(ApplicationManager.isConferenceMode() || validateLanguages(nativeLanguage, langCode)) {
                                    foreignLanguage = btTalk_f2f_1Lang = langCode;
                                    commitForeignLangLanguage(viewNativeLang_f2f_1, R.id.f2f_native_lang_img_1);
                                }
                            }
                        }
                    } else { //right
                        viewChangedLang = viewNativeLang_f2f_2;
                        if (btTalk_f2f_2Lang.equals(nativeLanguage)) {
                            if (connected) {
                                sendChangeLanguageRequest(langCode, btTalk_f2f_1Lang);
                            } else {
                                if(validateLanguages(langCode, foreignLanguage)) {
                                    nativeLanguage = btTalk_f2f_2Lang = langCode;
                                    commitNativeLangLanguage(viewNativeLang_f2f_2, R.id.f2f_native_lang_img_2);
                                }
                            }
                        } else {
                            if (connected) {
                                sendChangeLanguageRequest(btTalk_f2f_1Lang, langCode);
                            } else {
                                if(validateLanguages(nativeLanguage, langCode)) {
                                    foreignLanguage = btTalk_f2f_2Lang = langCode;
                                    commitForeignLangLanguage(viewNativeLang_f2f_2, R.id.f2f_native_lang_img_2);
                                }
                            }
                        }
                    }
                }
            }
        });

        AlertDialog dialog = languageMenu.create();
        dialog.show();
    }

    ///////////////////
    /// protected methods
    ///////////////////

    protected void logInfo(String message) {
        Log.i(TAG, message);
    }

    protected void logWarn(String message) {
        Log.w(TAG, message);
    }

    protected void logError(String message) {
        Log.e(TAG, message);
    }

    protected void handleTranslationResult(Map<String, String> resultMap) {
    }

    protected void setTag(String tag) {
        if (tag != null && !tag.isEmpty())
            TAG = tag;
    }

    protected void changeMode(Context packageContext, Class<?> cls) {
        Intent intent = new Intent(packageContext, cls);
        intent.putExtra("activity",cls.getSimpleName());
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }

    protected LexifoneSetting loadLexifoneSetting() {
        LexifoneSetting setting = new LexifoneSetting();
        setting.setUserName(userName);
        setting.setPassword(password);
        setting.setNativeLanguage(nativeLanguage);
        setting.setSampleRate(sampleRate);
        setting.setEncoded(encodeData);
        setting.setLexifoneAddress(lexifoneAddress);
        setting.setCustomerId(customerId);
        setting.setLatitude(latitude);
        setting.setLongitude(longitude);
        return setting;
    }

    protected boolean validateLanguages(String nativeLang, String foreignLang){
        String validationResult = lexifoneManager.validateLanguages(nativeLang, foreignLang);
        if (validationResult != null) {
            new AlertDialog.Builder(this)
                    .setTitle("Language selection")
                    .setMessage("Selected languages are not valid: " + validationResult)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    }).setIcon(android.R.drawable.ic_dialog_alert).show();
            return false;
        }else{
            return true;
        }
    }

    ///////////////////
    /// private methods
    ///////////////////

    private void handleChangeLanguagesResult(Intent intent) {
        if (LexifoneManager.CHANGE_SETTING_SUCCESS.equals(intent.getStringExtra(lexifoneManager.MSG_KEY))) {
            setLanguages();
        } else {
            displayToast(getText(R.string.change_languages_failed) + NEW_LINE + intent.getStringExtra(lexifoneManager.MSG_KEY));
        }
        toggleActionButtons(true);
        progressBar.setVisibility(View.GONE);
    }

    private void updateSettings() {
        playTranslation = sharedPreferences.getBoolean(SettingsActivity.PREF_PALY_TRANSLATION, false);
    }

    private void sendChangeLanguageRequest() {
        String prefNativeLanguage = sharedPreferences.getString(getNativeLanPrefKey(), null);
        if(prefNativeLanguage == null){
            displayToast("Cannot change language - failed to get preference native language");
            return;
        }

        if (ApplicationManager.isConferenceMode()) {
            if (!prefNativeLanguage.equals(nativeLanguage)) {
                sendChangeLanguageRequest(prefNativeLanguage, null);
            }
        } else {
            String prefForeignLanguage = sharedPreferences.getString(SettingsActivity.PREF_FOREIGN_LANG_NAME, null);
            if(prefForeignLanguage == null){
                displayToast("Cannot change language - failed to get preference foreign language");
                return;
            }
            if (!prefNativeLanguage.equals(btTalk_f2f_1Lang) || !prefForeignLanguage.equals(btTalk_f2f_2Lang)) {
                sendChangeLanguageRequest(prefNativeLanguage, prefForeignLanguage);
            }
        }
    }

    protected void sendChangeLanguageRequest(String updateNativeLang, String updatedForeignLang) {
        try {
            lexifoneManager.changeLanguages(updateNativeLang, updatedForeignLang == null ? null : new String[]{updatedForeignLang});
        } catch (Exception e) {
            handleError(e);
            toggleActionButtons(true);
            progressBar.setVisibility(View.GONE);
        }
    }

    private void setLanguages() {
        ApplicationManager.nativeLanguage = nativeLanguage = lexifoneManager.getNativeLanguage();
        if (ApplicationManager.isConferenceMode()) {
            btTalk_f2f_1Lang = nativeLanguage;
            commitNativeLangLanguage(viewNativeLang_f2f_1, R.id.f2f_native_lang_img_1);
        } else {
            String currentForeignLang = foreignLanguage;
            foreignLanguage = lexifoneManager.getForeignLanguages()[0];
            if (updateTalkButtons) {
                SharedPreferences.Editor editor = sharedPreferences.edit();
                if (viewChangedLang != null) {
                    if (viewChangedLang == viewNativeLang_f2f_1) {
                        String changedLang = btTalk_f2f_1Lang.equals(currentForeignLang) ? foreignLanguage : nativeLanguage;
                        btTalk_f2f_1Lang = changedLang;
                        viewNativeLang_f2f_1.setText(languageMap.get(changedLang).getDisplayName());
                        editor.putString(getNativeLanPrefKey(), changedLang);
                    } else {
                        String changedLang = btTalk_f2f_2Lang.equals(currentForeignLang) ? foreignLanguage : nativeLanguage;
                        btTalk_f2f_2Lang = changedLang;
                        viewNativeLang_f2f_2.setText(languageMap.get(changedLang).getDisplayName());
                        editor.putString(SettingsActivity.PREF_FOREIGN_LANG_NAME, changedLang);
                    }
                } else {
                    btTalk_f2f_1Lang = nativeLanguage;
                    btTalk_f2f_2Lang = foreignLanguage;
                    viewNativeLang_f2f_1.setText(languageMap.get(nativeLanguage).getDisplayName());
                    viewNativeLang_f2f_2.setText(languageMap.get(foreignLanguage).getDisplayName());
                    editor.putString(getNativeLanPrefKey(), nativeLanguage);
                    editor.putString(SettingsActivity.PREF_FOREIGN_LANG_NAME, foreignLanguage);
                }
                editor.commit();
                updateFlags();
                updateTalkButtons = false;
                viewChangedLang = null;
            }
        }
    }

    protected void commitNativeLangLanguage(TextView langView, int btnId) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(getNativeLanPrefKey(), nativeLanguage);
        editor.commit();
        langView.setText(languageMap.get(nativeLanguage).getDisplayName());
        updateFlag(nativeLanguage, btnId);
        ApplicationManager.nativeLanguage = nativeLanguage;
    }

    protected void commitForeignLangLanguage(TextView langView, int btnId) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(SettingsActivity.PREF_FOREIGN_LANG_NAME, foreignLanguage);
        editor.commit();
        langView.setText(languageMap.get(foreignLanguage).getDisplayName());
        updateFlag(foreignLanguage, btnId);
    }

    protected void updateFlag(String langCode, int buttonId) {
        String fileName = langCode.replace("-", "_");
        int flagId = getResources().getIdentifier(fileName, "drawable", packageName);
        displayFlag(flagId, buttonId);
    }

    private void updateFlags() {
        String langCode_f2f_1 = btTalk_f2f_1Lang;
        String langCode_f2f_2 = btTalk_f2f_2Lang;

        String fileName = null;
        if (langCode_f2f_1 != null) {
            fileName = langCode_f2f_1.replace("-", "_");
            int flagId_1 = getResources().getIdentifier(fileName, "drawable", BaseActivity.this.getPackageName());
            displayFlag(flagId_1, R.id.f2f_native_lang_img_1);

        }
        if (ApplicationManager.isFace2FaceMode() && langCode_f2f_2 != null) {
            fileName = langCode_f2f_2.replace("-", "_");
            int flagId_2 = getResources().getIdentifier(fileName, "drawable", BaseActivity.this.getPackageName());
            displayFlag(flagId_2, R.id.f2f_native_lang_img_2);
        }
    }

    protected void displayFlag(int fileNameResource, int imgViewId) {
        ImageView flag = (ImageView) findViewById(imgViewId);
        if (fileNameResource == 0) {
            flag.setImageResource(R.drawable.world_icon);
        } else {
            flag.setImageResource(fileNameResource);
        }
    }

    private void handleConnectStatusMsg(Intent intent) {
        try {
            LexifoneManager.ConnectionStatus status = LexifoneManager.ConnectionStatus.valueOf(intent.getStringExtra(lexifoneManager.MSG_KEY));
            btTalk_f2f_1.setEnabled(false);
            if (ApplicationManager.isFace2FaceMode()) {
                btTalk_f2f_2.setEnabled(false);
            }
            switch (status) {
                case CONNECTED_AND_VALIDATED:
                    setTitle(R.string.user_validate);
                    switchStartStop.setText(getString(R.string.lexifone_stop));
                    switchStartStop.setEnabled(true);
                    toggleActionButtons(true);
                    progressBar.setVisibility(View.GONE);
                    break;
                case NOT_CONNECTED:
                    setTitle(R.string.disconnected);
                    switchStartStop.setText(getString(R.string.lexifone_start));
                    switchStartStop.setEnabled(true);
                    switchStartStop.setChecked(false);
                    toggleActionButtons(false);
                    progressBar.setVisibility(View.GONE);
                    break;
                case CONNECTING:
                    setTitle(R.string.lexifone_connecting);
                    switchStartStop.setText(getString(R.string.lexifone_connecting));
                    switchStartStop.setEnabled(false);
                    break;
                case DISCONNECTING:
                    setTitle(R.string.lexifone_disconnecting);
                    switchStartStop.setText(getString(R.string.lexifone_disconnecting));
                    switchStartStop.setEnabled(false);
                    toggleActionButtons(false);
                    break;
            }
        } catch (Exception e) {
            setTitle(intent.getStringExtra(lexifoneManager.MSG_KEY));
            displayToast(R.string.invalid_connect_status + intent.getStringExtra(lexifoneManager.MSG_KEY), Toast.LENGTH_LONG);
        }
    }


//    @Override
//    public void setTitle(int titleId) {
//        setTitle(getText(titleId).toString());
//    }
//
//    @Override
//    public void setTitle(CharSequence title) {
//        int pref = ApplicationManager.isConferenceMode() ? R.string.conference_mode : R.string.face_to_face_mode;
//        super.setTitle(getText(pref).toString() + " - " + title);
//    }
//
    private void handleTranslateResult(Intent intent) {
        String transcript = intent.getStringExtra(lexifoneManager.NATIVE_TRANSCRIPT_KEY);
        if (LexifoneManager.AUDIO_INTERPETER_NOMATCH.equals(transcript)) {
            displayToast(R.string.voice_recognition_failed, Toast.LENGTH_LONG);
        } else {
            Bundle bundle = intent.getBundleExtra(lexifoneManager.MSG_KEY);
            Map<String, String> map = (Map<String, String>) bundle.getSerializable(LexifoneManager.TEXT_RESULT_KEY);
            handleTranslationResult(map);
        }
    }

    private void handleSystemMessage(Intent intent) {
        String message = intent.getStringExtra(lexifoneManager.MSG_KEY);
        if (LexifoneManager.RESPONSE_INVALID.equals(message)) {
            progressBar.setVisibility(View.GONE);
        }
        String description = intent.getStringExtra(lexifoneManager.SYSTEM_MSG_DESCRIPTION);
        if (description != null && description.length() > 0) {
            message += NEW_LINE + description;
        }
        displayToast(message, Toast.LENGTH_LONG);
    }

    private void startCaptureAudio() {
        Log.e(TAG, "startCaptureAudio");
        this.captureAudio = true;
        new Thread(new Runnable() {
            public void run() {
                if (lexifoneManager.isEncoded()) {
                    recordAndSendEncoded();
                } else { // send audio not encoded - currently disabled in settings
                    recordAndSendAudio();
                }
            }
        }).start();
    }

    private void recordAndSendEncoded() {
        AudioRecord recorder = null;
        try {
            int sampleRate = lexifoneManager.getSampleRate();
            int bufferSize = 16000;// AudioRecord.getMinBufferSize(sampleRate, CHANNEL_IN_CONFIG, AUDIO_FORMAT) * 10;
            recorder = new AudioRecord(AUDIO_SOURCE, sampleRate, CHANNEL_IN_CONFIG, AUDIO_FORMAT, bufferSize);
            int status = -1;
            short audioData[] = new short[bufferSize];
            recorder.startRecording();
            while (captureAudio) {
                status = recorder.read(audioData, 0, audioData.length);
                if (status <= 0) {
                    continue;
                }
                try {
                    lexifoneManager.sendAudioEncoded(audioData, 0, status);
                } catch (Exception e) {
                    Log.e(TAG, "exception sending data: " + e.getLocalizedMessage());
                }
                audioData = new short[bufferSize];
            }
            recorder.stop();
            try { // handle the recorded left over
                Thread.sleep(100);
                while ((status = recorder.read(audioData, 0, audioData.length)) > 0) {
                    lexifoneManager.sendAudioEncoded(audioData, 0, status);
                    audioData = new short[bufferSize];
                }
            } catch (Exception e) {
                handleError("exception while finish reading recorded data: " + e.getLocalizedMessage(), e);
            }
        } catch (Exception e) {
            handleError("exception while reading recorded data: " + e.getLocalizedMessage(), e);
        } finally {
            if (recorder != null) {
                try {
                    recorder.release();
                } catch (Exception e) {
                    Log.e(TAG, "exception recorder.release - " + e.getLocalizedMessage());
                }
            }
            audioReadingFinished = true;
        }
    }

    private void recordAndSendAudio() {
        int sampleRate = lexifoneManager.getSampleRate();
        int buffersize = AudioRecord.getMinBufferSize(sampleRate, CHANNEL_IN_CONFIG, AUDIO_FORMAT);
        AudioRecord recorder = new AudioRecord(AUDIO_SOURCE, sampleRate, CHANNEL_IN_CONFIG, AUDIO_FORMAT, buffersize);
        int status = -1;
        recorder.startRecording();
        byte audioData[] = new byte[buffersize];
        while (captureAudio) {
            status = recorder.read(audioData, 0, audioData.length);
            if (status == AudioRecord.ERROR_INVALID_OPERATION || status == AudioRecord.ERROR_BAD_VALUE) {
                continue;
            }
            try {
                lexifoneManager.sendAudio(audioData);
            } catch (Exception e) {
                Log.e(TAG, "exception sending data: " + e.getLocalizedMessage());
            }
            audioData = new byte[buffersize];
        }
        recorder.stop();
        recorder.release();
    }

    protected void stopCaptureAudio() {
        Log.e(TAG, "stopCaptureAudio");
        try {
            Thread.sleep(STOP_CAPTURE_WAIT);
            audioReadingFinished = false;
            captureAudio = false;
            int times = 10 * 2;
            for (int i = 0; i < times && !audioReadingFinished; i++) {
                Thread.sleep(100);
            }
            if (!audioReadingFinished) {
                throw new RuntimeException("failed to stop AudioRecorder reading");
            }
        } catch (InterruptedException e) {
            Log.e(TAG, "stopCaptureAudio - InterruptedException");
        }
    }

    private void playMedia(byte[] data, int sampleRate) {
        try {
            int minBuffSize = AudioTrack.getMinBufferSize(sampleRate, TEST_CONF, TEST_FORMAT);
            AudioTrack track = new AudioTrack(TEST_STREAM_TYPE, sampleRate, TEST_CONF, TEST_FORMAT, minBuffSize, TEST_MODE);
            byte[] temp = new byte[minBuffSize];
            ByteArrayInputStream ios = new ByteArrayInputStream(data);
            int byteRead = ios.read(temp);
            track.play();
            while (byteRead > 0) {
                track.write(temp, 0, byteRead);
                byteRead = ios.read(temp);
            }

            track.release();

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void displayToast(String message) {
        displayToast(message, Toast.LENGTH_SHORT);
    }

    protected void displayToast(int resId) {
        displayToast(getText(resId).toString(), Toast.LENGTH_SHORT);
    }

    protected void displayToast(int resId, int duration) {
        displayToast(getText(resId).toString(), duration);
    }

    protected void displayToast(String message, int duration) {
        Context context = getApplicationContext();
        if (context == null || message == null || message.length() == 0) {
            return;
        }
        Toast toast = Toast.makeText(context, message, duration);
        toast.show();
    }

    protected void registerListeners() {
        LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(this);
        IntentFilter filter = new IntentFilter(lexifoneManager.EVENT_CONNECT_FILTER);
        filter.addAction(lexifoneManager.EVENT_CHANGE_LANGS_FILTER);
        broadcastManager.registerReceiver(mStatusMessageReceiver, filter);

        broadcastManager.registerReceiver(mSystemMessageReceiver, new IntentFilter(lexifoneManager.EVENT_SYSTEM_MSG_FILTER));
        broadcastManager.registerReceiver(mAudioMessageReceiver, new IntentFilter(lexifoneManager.EVENT_AUDIO_DATA_FILTER));
        broadcastManager.registerReceiver(mTranslationMessageReceiver, new IntentFilter(lexifoneManager.EVENT_TRANSLATE_RESULT_FILTER));
    }

    protected void unregisterListeners() {
        // Unregister since the activity is about to be closed.
        LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(this);
        broadcastManager.unregisterReceiver(mStatusMessageReceiver);
        broadcastManager.unregisterReceiver(mTranslationMessageReceiver);
        broadcastManager.unregisterReceiver(mAudioMessageReceiver);
    }

    protected void init() {
        packageName = BaseActivity.this.getPackageName();
        preferencesInit();
        uiInit();
        updateFlags();
        toggleActionButtons(false);
    }

    private void preferencesInit() {
        userName = sharedPreferences.getString(SettingsActivity.PREF_USER_NAME, null);
        password = sharedPreferences.getString(SettingsActivity.PREF_PASSWORD, null);
        lexifoneAddress = sharedPreferences.getString(SettingsActivity.PREF_LEXIFONE_ADRESS, null);
        if (languageMap == null) {
            languageMap = lexifoneManager.getLanguaeDescriptorMap(lexifoneAddress);
            reverseLanguageMap = new TreeMap<>();
            languageDescriptionArray = new String[languageMap.size()];
            for (LanguageDescriptor lang : languageMap.values()) {
                if (lang.isBeta()) {
                    reverseLanguageMap.put(lang.getDisplayName() + BaseActivity.BETA_SUFFIX, lang.getLanguageCode());
                } else {
                    reverseLanguageMap.put(lang.getDisplayName(), lang.getLanguageCode());
                }
            }
            reverseLanguageMap.keySet().toArray(languageDescriptionArray);
        }

        encodeData = sharedPreferences.getBoolean(SettingsActivity.PREF_ENCODE_DATA, false);
        customerId = sharedPreferences.getString(SettingsActivity.PREF_CUSTOMER_ID, null);
        sampleRate = Integer.parseInt(sharedPreferences.getString(SettingsActivity.PREF_SAMPLE_RATE, String.valueOf(DEFAULT_SAMPLE_RATE)));
        String nativeLangPrefKey = getNativeLanPrefKey();
        if(ApplicationManager.nativeLanguage != null) {
            commitPrefernce(nativeLangPrefKey, ApplicationManager.nativeLanguage);
        }
        ApplicationManager.nativeLanguage = nativeLanguage = getLanguage(nativeLangPrefKey, R.string.default_native_lang);
        foreignLanguage = getLanguage(SettingsActivity.PREF_FOREIGN_LANG_NAME, R.string.default_foreign_lang);
        playTranslation = sharedPreferences.getBoolean(SettingsActivity.PREF_PALY_TRANSLATION, false);

        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if (location != null) {
            longitude = location.getLongitude();
            latitude = location.getLatitude();
        }
        speakerId = sharedPreferences.getString(SettingsActivity.PREF_SPEAKER_ID, null);
        if (speakerId == null) {
            speakerId = UUID.randomUUID().toString();
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(SettingsActivity.PREF_SPEAKER_ID, speakerId);
            editor.commit();
        }
    }

    protected String getNativeLanPrefKey(){
        return ApplicationManager.isConferenceMode() ? SettingsActivity.PREF_CONF_NATIVE_LANG_NAME : SettingsActivity.PREF_F2F_NATIVE_LANG_NAME;
    }

    private String getLanguage(String prefKey, int defaultValueId) {
        try {
            String language = sharedPreferences.getString(prefKey, null);
            if (language == null || languageMap.get(language) == null) {
                language = getResources().getString(defaultValueId);
                if(language == null){
                    handleError("failed to get default language");
                    return "en-us";
                }
                language = language.replace("_", "-");
                displayToast("reset language to default: " + language);
            }
            commitPrefernce(prefKey, language);
            return language;
        }catch (Exception e){
            handleError("failed to set language", e);
            return "en-us";
        }
    }

    protected void commitPrefernce(String prefKey, String prefValue){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(prefKey, prefValue);
        editor.commit();
    }

    protected void uiInit() {
        setTitle(getString(R.string.disconnected));

        EditNativeText = (CustomEditText) findViewById(R.id.f2f_native_text_et);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        TextView modeView = (TextView)findViewById(R.id.mode_tv);
        modeView.setText(ApplicationManager.isConferenceMode() ? getString(R.string.conference_mode) : getString(R.string.face_to_face_mode));
        switchStartStop = (Switch) findViewById(R.id.start_stop_switch);
        switchStartStop.setFocusable(true);
        switchStartStop.setFocusableInTouchMode(true);
        switchStartStop.requestFocus();
        switchStartStop.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                progressBar.setVisibility(View.VISIBLE);
                if (isChecked) {
                    try {
                        if (lexifoneManager.notConnected()) {
                            lexifoneManager.start(getApplicationContext(), loadLexifoneSetting(), DEFAULT_CONNECTION_TIMEOUT_SECONDS);
                        } else {
                            Log.i(TAG, "start_stop - do nothing, not connected");
                        }
                    } catch (Exception e) {
                        progressBar.setVisibility(View.GONE);
                        setTitle(e.getLocalizedMessage());
                        switchStartStop.setChecked(false);
                        handleError(e);
                    }
                } else {
                    try {
                        lexifoneManager.stop();
                    } catch (Exception e) {
                        Log.e(TAG, "failed to stop lexifoneManager");
                    }
                }
            }
        });
        viewNativeLang_f2f_1 = (TextView) findViewById(R.id.f2f_native_lang_title_tv_1);
        viewNativeLang_f2f_1.setText(languageMap.get(nativeLanguage).getDisplayName());
        viewNativeLang_f2f_2 = (TextView) findViewById(R.id.f2f_native_lang_title_tv_2);
        viewNativeLang_f2f_2.setText(languageMap.get(foreignLanguage).getDisplayName());

        btTalk_f2f_1 = (ImageButton) findViewById(R.id.f2f_talk_bt_1);
        btTalk_f2f_1.setBackgroundResource(R.drawable.audio_button_normal);
        btTalk_f2f_1.setOnTouchListener(this);
        btTalk_f2f_1Lang = nativeLanguage;


        btTalk_f2f_2 = (ImageButton) findViewById(R.id.f2f_talk_bt_2);
        btTalk_f2f_2.setBackgroundResource(R.drawable.audio_button_normal_2);
        btTalk_f2f_2.setOnTouchListener(this);
        btTalk_f2f_2Lang = foreignLanguage;

        EditNativeText = (CustomEditText) findViewById(R.id.f2f_native_text_et);
        EditNativeText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                changeToWriteModeBtn();

                scrollPageDown();

                EditNativeText.setOnTouchListener(null);
                return false;
            }
        });
        EditNativeText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeToWriteModeBtn();
                scrollPageDown();
            }
        });

        EditNativeText.setOnBackKeyPressedEventListener(new OnBackKeyPressedEventListener() {
            @Override
            public void onEvent() {
                changeToMicModeBtn();
                scrollPageDown();
            }
        });
    }

    public void changeToMicModeBtn() {
        btTalk_f2f_1.setImageResource(R.drawable.microphone_white);
        btTalk_f2f_1.setOnTouchListener(BaseActivity.this);
        btTalk_f2f_2.setImageResource(R.drawable.microphone_black);
        btTalk_f2f_2.setOnTouchListener(BaseActivity.this);
    }

    public void changeToWriteModeBtn() {
        btTalk_f2f_1.setImageResource(R.drawable.send_white);
        btTalk_f2f_1.setOnTouchListener(null);
        btTalk_f2f_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                translateTextAndGoToMicMode(v);
            }
        });
        btTalk_f2f_2.setImageResource(R.drawable.send_black);
        btTalk_f2f_2.setOnTouchListener(null);
        btTalk_f2f_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                translateTextAndGoToMicMode(v);
            }
        });
    }

    public void translateTextAndGoToMicMode(View v) {
        translateText(v);
        hideKeyboard();
        changeToMicModeBtn();
    }

    public void translateText(View view) {
        if (ApplicationManager.isConferenceMode()) {
            base_translateText(view, nativeLanguage, null);
        } else {
            if (view.getId() == R.id.f2f_talk_bt_1) {
                base_translateText(view, btTalk_f2f_1Lang, new String[]{btTalk_f2f_2Lang});
            } else {
                base_translateText(view, btTalk_f2f_2Lang, new String[]{btTalk_f2f_1Lang});
            }
        }
    }

    private void toggleActionButtons(boolean enable) {
        try {
            btTalk_f2f_1.setEnabled(enable);
            btTalk_f2f_2.setEnabled(enable);
        } catch (Exception e) {
            handleError("toggleActionButtons - exception:" + e.getLocalizedMessage(), e);
        }
    }

//    private String getDeviceUniqueId() {
//        String identifier = null;
//        TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
//        if (tm != null) {
//            identifier = tm.getDeviceId();
//        }
//        if (identifier == null || identifier.length() == 0) {
//            identifier = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
//        }
//        return identifier;
//    }

    protected void handleError(Exception e) {
        e.printStackTrace();
        handleError(e.getMessage());
    }

    protected void handleError(String messager, Exception e) {
        e.printStackTrace();
        handleError(messager);
    }

    protected void handleError(String message) {
        Log.e(TAG, "Error in application: " + message);
        displayToast(message);
    }

    protected void inflateBubbleTranslationRight(String userName, String originalText, String translateText, int contentLayoutId, String languageToSpeak) {

        LayoutInflater inf = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View newBubbleLayout = inf.inflate(R.layout.text_bubble_right, null);

        TextView userTv = (TextView) newBubbleLayout.findViewById(R.id.userNameRightTv);
        TextView originalTv = (TextView) newBubbleLayout.findViewById(R.id.upRightTv);
        TextView translateTv = (TextView) newBubbleLayout.findViewById(R.id.downRightTv);
        ImageView playAudio = (ImageView) newBubbleLayout.findViewById(R.id.playingRightImg);

        if (userName != null)
            userTv.setText(userName + timeStamp());
        else
            userTv.setText("user 2" + timeStamp());

        if (originalText == null || originalText.isEmpty()) {
            originalText = "empty transcript";
        }
        if (translateText == null || translateText.isEmpty()) {
            originalText = "empty translation";
        }
        originalTv.setText(originalText);
        translateTv.setText(translateText);
        translateTv.setTag(languageToSpeak);
        playAudio.setOnClickListener(playAudioListener);

        LinearLayout contentLayout = (LinearLayout) findViewById(contentLayoutId);
        contentLayout.addView(newBubbleLayout);
        scrollPageDown();
    }

    protected void inflateBubbleTranslationLeft(String userName, String originalText, String translateText, int contentLayoutId, String languageToSpeak) {
        LayoutInflater inf = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View newBubbleLayout = inf.inflate(R.layout.text_bubble_left, null);

        TextView userTv = (TextView) newBubbleLayout.findViewById(R.id.userNameLeftTv);
        TextView originalTv = (TextView) newBubbleLayout.findViewById(R.id.upLeftTv);
        TextView translateTv = (TextView) newBubbleLayout.findViewById(R.id.downLeftTv);
        ImageView playAudio = (ImageView) newBubbleLayout.findViewById(R.id.playingLeftImg);

        if (userName != null) {
            userTv.setText(userName + timeStamp());
        } else {
            userTv.setText("user 1" + timeStamp());
        }

        if (originalText == null || originalText.isEmpty()) {
            originalText = "";
        }

        playAudio.setVisibility(View.GONE);
        if (translateText == null || translateText.isEmpty()) {
            translateText = "";
        } else if (languageToSpeak != null) {
            translateTv.setTag(languageToSpeak);
            playAudio.setOnClickListener(playAudioListener);
            playAudio.setVisibility(View.VISIBLE);
        }

        originalTv.setText(originalText);
        translateTv.setText(translateText);

        LinearLayout contentLayout = (LinearLayout) findViewById(contentLayoutId);
        contentLayout.addView(newBubbleLayout);
        scrollPageDown();
    }

    protected void scrollPageDown() {

        final ScrollView scrollView = (ScrollView) findViewById(R.id.contentScrollView);


        Handler handler = new Handler();

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                scrollView.post(new Runnable() {
                    @Override
                    public void run() {
                        scrollView.fullScroll(ScrollView.FOCUS_DOWN);
                    }
                });
            }
        }, 300);
    }


    protected OnClickListener playAudioListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            try {
                ViewGroup parentView = (ViewGroup) v.getParent();
                TextView textToTranslateTv;
                String textToTranslate;

                if (parentView.getId() == R.id.leftBubbleMainLayout) {
                    textToTranslateTv = (TextView) parentView.findViewById(R.id.downLeftTv);
                    textToTranslate = textToTranslateTv.getText().toString();
                    lexifoneManager.textToSpeech(textToTranslateTv.getTag().toString(), textToTranslate);

                } else if (parentView.getId() == R.id.rightBubbleMainLayout) {
                    textToTranslateTv = (TextView) parentView.findViewById(R.id.downRightTv);
                    textToTranslate = textToTranslateTv.getText().toString();
                    lexifoneManager.textToSpeech(textToTranslateTv.getTag().toString(), textToTranslate);
                }
            }catch (Exception e){
                handleError("playAudioListener::onClick - error", e);
            }
        }
    };


    public void hideKeyboard() {
        if (getWindow().getCurrentFocus() != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getWindow().getCurrentFocus().getWindowToken(), 0);
        }
    }

    public void cleanDialog(int massage, int title, final ViewGroup content) {

        new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(massage)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        content.removeAllViews();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        return;
                    }
                })

                .setIcon(R.drawable.clean_icon)
                .show();

    }


}
