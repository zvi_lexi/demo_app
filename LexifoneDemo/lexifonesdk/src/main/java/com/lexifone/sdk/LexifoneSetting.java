package com.lexifone.sdk;

import java.util.Arrays;

/**
 * the class represents the setting neededby LexifoneManaager
 * to process application requests
 */
public class LexifoneSetting {

    private String mediaServerAddress;
    private String nativeLanguage;
    private String[] foreignLanguages;
    private String userName;
    private String password;
    private boolean performTTS;
    private int sampleRate;
    private String messageType;
    private boolean encoded = true;
    private String speakerId;
    private String lexifoneAddress;
    private String customerId;
    private String globalSessionId;
    private String displayName;
    private double longitude;
    private double latitude;

    public int getConnectionTimeout() {
        return connectionTimeout;
    }

    public void setConnectionTimeout(int connectionTimeout) {
        this.connectionTimeout = connectionTimeout;
    }

    private int connectionTimeout;

    public String getMediaServerAddress() {
        return mediaServerAddress;
    }

    public void setMediaServerAddress(String mediaServerAddress) {
        this.mediaServerAddress = mediaServerAddress;
    }

    public String getNativeLanguage() {
        return nativeLanguage;
    }

    public void setNativeLanguage(String nativeLanguage) {
        this.nativeLanguage = nativeLanguage;
    }

    public String[] getForeignLanguages() {
        return foreignLanguages;
    }

    public void setForeignLanguages(String[] foreignLanguages) {
        this.foreignLanguages = foreignLanguages;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean getPerformTTS() {
        return performTTS;
    }

    public void setPerformTTS(boolean flag) {
        this.performTTS = flag;
    }

    public int getSampleRate() {
        return sampleRate;
    }

    public void setSampleRate(int sampleRate) {
        this.sampleRate = sampleRate;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String type) {
        messageType = type;
    }

    public boolean isEncoded() {
        return encoded;
    }

    public void setEncoded(boolean encoded) {
        this.encoded = encoded;
    }

    public String getSpeakerId() {
        return speakerId;
    }

    public void setSpeakerId(String speakerId) {
        this.speakerId = speakerId;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getGlobalSessionId() {
        return globalSessionId;
    }

    public void setGlobalSessionId(String globalSessionId) {
        this.globalSessionId = globalSessionId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getLexifoneAddress() { return lexifoneAddress; }

    public void setLexifoneAddress(String lexifoneAddress) {
        this.lexifoneAddress = lexifoneAddress;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public boolean isPerformTTS() {
        return performTTS;
    }

    @Override
    public String toString() {
        return "LexifoneSetting{" +
                "mediaServerAddress='" + mediaServerAddress + '\'' +
                ", nativeLanguage='" + nativeLanguage + '\'' +
                ", foreignLanguages=" + Arrays.toString(foreignLanguages) +
                ", userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                ", performTTS=" + performTTS +
                ", sampleRate=" + sampleRate +
                ", messageType='" + messageType + '\'' +
                ", encoded=" + encoded +
                ", speakerId='" + speakerId + '\'' +
                ", lexifoneAddress='" + lexifoneAddress + '\'' +
                ", customerId='" + customerId + '\'' +
                ", globalSessionId='" + globalSessionId + '\'' +
                ", displayName='" + displayName + '\'' +
                ", longitude='" + longitude + '\'' +
                ", latitude='" + latitude +
                '}';
    }
}