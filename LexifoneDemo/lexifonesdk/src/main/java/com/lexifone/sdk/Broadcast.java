package com.lexifone.sdk;

/**
 * Created by Zvi on 5/13/2015.
 */

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;

class Broadcast {
    private Context context;

    Broadcast(Context context){
        this.context = context;
    }

    public void sendSystemMessage(String message) {
        Bundle bundle = new Bundle();
        bundle.putString(LexifoneManager.MSG_KEY, message);
        send(bundle, LexifoneManager.EVENT_SYSTEM_MSG_FILTER);
    }

    public void sendSystemMessage(String message, String code, String description){
        Bundle bundle = new Bundle();
        bundle.putString(LexifoneManager.MSG_KEY, message);
        bundle.putString(LexifoneManager.SYSTEM_MSG_CODE, code);
        bundle.putString(LexifoneManager.SYSTEM_MSG_DESCRIPTION, description);
        send(bundle, LexifoneManager.EVENT_SYSTEM_MSG_FILTER);
    }

    public void sendConnectStatus(String status){
        Bundle bundle = new Bundle();
        bundle.putString(LexifoneManager.MSG_KEY, status);
        send(bundle, LexifoneManager.EVENT_CONNECT_FILTER);
    }

    public void sendChangeLanguagesStatus(String status){
        Bundle bundle = new Bundle();
        bundle.putString(LexifoneManager.MSG_KEY, status);
        send(bundle, LexifoneManager.EVENT_CHANGE_LANGS_FILTER);
    }

    public void sendTranslateResultMsg(String transcript, Bundle resultBundle){
        Bundle bundle = new Bundle();
        bundle.putString(LexifoneManager.NATIVE_TRANSCRIPT_KEY, transcript);
        bundle.putBundle(LexifoneManager.MSG_KEY, resultBundle);
        send(bundle, LexifoneManager.EVENT_TRANSLATE_RESULT_FILTER);
    }

    public void sendAudioData(byte[] data){
        Bundle bundle = new Bundle();
        bundle.putByteArray(LexifoneManager.MSG_KEY, data);
        send(bundle, LexifoneManager.EVENT_AUDIO_DATA_FILTER);
    }

    private void send(Bundle bundle, String intentFilter){
        if(context == null){
            return;
        }
        Intent intent = new Intent(intentFilter);
        intent.putExtras(bundle);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }
}
