package com.lexifone.sdk;

/**
 * contains language details for language/dialect code.
 */
public class LanguageDescriptor {

    private String languageCode;
    private String displayName;
    private String languageName;
    private boolean beta;

    LanguageDescriptor(String languageCode, String displayName, String languageName, boolean beta) {
        this.languageCode = languageCode;
        this.displayName = displayName;
        this.languageName = languageName;
        this.beta = beta;
    }

    /**
     * get the language/dialect display name.
     * @return String - display name.
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * get the language/dialect beta flag. "Beta" languages are publicly available,
     * however feedback from real world cases will help us improve these languages over time.
     * @return true if language/dialect is defined as beta.
     */
    public boolean isBeta() {
        return beta;
    }

    /**
     * get the language/dialect code.
     * @return String - the language code.
     */
    public String getLanguageCode() {
        return languageCode;
    }

    /**
     * get the language name (a language name can be common
     * to several language codes, e.g. "English" is the language name
     * of "en-us","en-uk","en-au" and "en-in" language codes).
     * @return String - the language name.
     */
    public String getLanguageName() {
        return languageName;
    }
}
