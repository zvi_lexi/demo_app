package com.lexifone.sdk;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Zvi on 5/19/2015.
 */
class Language {
    private static Map<String, LanguageDescriptor> languageDescriptorMap = new HashMap<String, LanguageDescriptor>();
    @Deprecated
    private static Map<String,String> dialectMap = new HashMap<String,String>();


    private static boolean languageRefreshed = false;

    static {
        load();
    }

    public static Map<String, LanguageDescriptor> getLanguageDescriptorMap() {
        return new HashMap<>(languageDescriptorMap);
    }

   @Deprecated
    public static Map<String, String> getDialectMap(){
      return dialectMap;
    }

    public static boolean isLanguageRefreshed() {
        return languageRefreshed;
    }

    public static void refreshListOfLanguages(List<Map<String, Object>> languageList) {
        Map<String, LanguageDescriptor> temp = new HashMap<String, LanguageDescriptor>();
        for (Map<String, Object> languageInfo : languageList) {
            String languageCode = (String) languageInfo.get("languageCode");
            String displayName = (String) languageInfo.get("displayName");
            String languageName = (String) languageInfo.get("languageName");
            Boolean beta = (Boolean) languageInfo.get("beta");
            if (beta == null) {
                beta = false;
            }
            temp.put(languageCode, new LanguageDescriptor(languageCode, displayName, languageName, beta));
        }
        updateDeprecatedDialectMap();
        languageDescriptorMap = temp;
        languageRefreshed = true;
    }


    public static boolean isValidDialect(String dialect) {
        return languageDescriptorMap.get(dialect) != null;
    }

//    <entry key="en-us" value="eng-USA" /> <!-- English -->
//    <entry key="en-gb" value="eng-GBR" /> <!-- English UK -->
//    <entry key="en-au" value="eng-AUS" /> <!-- English Australian -->
//    <entry key="es-es" value="spa-ESP" /> <!-- Spanish -->
//    <entry key="es-mx" value="spa-XLA" /> <!-- Spanish Americas -->
//    <entry key="de-de" value="deu-DEU" /> <!-- German -->
//    <entry key="it-it" value="ita-ITA" /> <!-- Italian -->
//    <entry key="fr-fr" value="fra-FRA" /> <!-- French -->
//    <entry key="fr-ca" value="fra-CAN" /> <!-- French Canadian -->
//    <entry key="pt-br" value="por-BRA" /> <!-- Portuguese Brazil -->
//    <entry key="pt-pt" value="por-PRT" /> <!-- Portuguese -->
//    <entry key="zh-cn" value="cmn-CHN" /> <!-- Mandarine -->
//    <entry key="ru-ru" value="rus-RUS" /> <!-- Russian -->
//    <entry key="he-il" value="heb-ISR" /> <!-- Hebrew -->
//    <entry key="pl-pl" value="pol-POL" /> <!-- Polish -->
//    <entry key="zh-tw" value="cmn-TWN" /> <!-- Taiwanese Mandarin -->
//    <entry key="en-in" value="eng-IND" /> <!-- Indian English -->

    private static void load() {
        
//        languageDescriptorMap.put("ar-eg", new LanguageDescriptor("ar-eg", "Arabic Egypt", "Arabic", true));  
//        languageDescriptorMap.put("ar-ae", new LanguageDescriptor("ar-ae", "Arabic International", "Arabic", true));
//        languageDescriptorMap.put("ar-sa", new LanguageDescriptor("ar-sa", "Arabic Saudi", "Arabic", true));
        languageDescriptorMap.put("en-au", new LanguageDescriptor("en-au", "English Australia", "English", false));
        languageDescriptorMap.put("en-in", new LanguageDescriptor("en-in", "English India", "English", false));
        languageDescriptorMap.put("en-gb", new LanguageDescriptor("en-gb", "English UK", "English", false));
        languageDescriptorMap.put("en-us", new LanguageDescriptor("en-us", "English US", "English", false));
        languageDescriptorMap.put("fr-ca", new LanguageDescriptor("fr-ca", "French Canada", "French", false));
        languageDescriptorMap.put("fr-fr", new LanguageDescriptor("fr-fr", "French Europe", "French", false));
        languageDescriptorMap.put("de-de", new LanguageDescriptor("de-de", "German", "German", false));
//        languageDescriptorMap.put("el-gr", new LanguageDescriptor("el-gr", "Greek", "Greek", true));
        languageDescriptorMap.put("he-il", new LanguageDescriptor("he-il", "Hebrew", "Hebrew", false));
//        languageDescriptorMap.put("hi-in", new LanguageDescriptor("hi-in", "Hindi", "Hindi", true));
        languageDescriptorMap.put("it-it", new LanguageDescriptor("it-it", "Italian", "Italian", false));
//        languageDescriptorMap.put("ja-jp", new LanguageDescriptor("ja-jp", "Japanese", "Japanese", true));
//        languageDescriptorMap.put("ko-kr", new LanguageDescriptor("ko-kr", "Korean", "Korean", true));
        languageDescriptorMap.put("zh-cn", new LanguageDescriptor("zh-cn", "Mandarin China", "Mandarin", false));
        languageDescriptorMap.put("zh-tw", new LanguageDescriptor("zh-tw", "Mandarin Taiwan", "Mandarin", false));
        languageDescriptorMap.put("pl-pl", new LanguageDescriptor("pl-pl", "Polish", "Polish", false));
        languageDescriptorMap.put("pt-br", new LanguageDescriptor("pt-br", "Portuguese Brazil", "Portuguese", false));
        languageDescriptorMap.put("pt-pt", new LanguageDescriptor("pt-pt", "Portuguese Europe", "Portuguese", false));
        languageDescriptorMap.put("ru-ru", new LanguageDescriptor("ru-ru", "Russian", "Russian", false));
        languageDescriptorMap.put("es-es", new LanguageDescriptor("es-es", "Spanish Europe", "Spanish", false));
        languageDescriptorMap.put("es-mx", new LanguageDescriptor("es-mx", "Spanish Latin America", "Spanish", false));
        updateDeprecatedDialectMap();

    }
    private static void updateDeprecatedDialectMap(){
        dialectMap.clear();
        for(Map.Entry<String, LanguageDescriptor> entry : languageDescriptorMap.entrySet()){
            dialectMap.put(entry.getKey() , entry.getValue().getDisplayName());
        }
    }

}
