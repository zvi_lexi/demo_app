package com.lexifone.sdk;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.purplefrog.speexjni.FrequencyBand;
import com.purplefrog.speexjni.SpeexDecoder;
import com.purplefrog.speexjni.SpeexEncoder;

import org.codehaus.jackson.map.ObjectMapper;

import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import de.tavendo.lexifone.WebSocket;
import de.tavendo.lexifone.WebSocketConnection;
import de.tavendo.lexifone.WebSocketOptions;
import io.fabric.sdk.android.Fabric;

/**
 * this class provides access to lexifone sdk functionality.
 * the calling application must use the getInstance method
 * to get a reference to its singletone object and then use
 * its public methods to process application requests.
 */
public class LexifoneManager {

    /**
     * used as intent filter when broadcasting system message.
     * Should be used as broadcast receiver filter to get only system messages.
     */
    public static final String EVENT_SYSTEM_MSG_FILTER = "system.message";

    /**
     * used as intent filter when broadcasting connection status message.
     * Should be used as broadcast receiver filter to get only connection status messages.
     */
    public static final String EVENT_CONNECT_FILTER = "connect.status";

    /**
     * used as intent filter when broadcasting change language status message.
     * Should be used as broadcast receiver filter to get only change language status messages.
     */
    public static final String EVENT_CHANGE_LANGS_FILTER = "change.languages.status";

    /**
     * used as intent filter when broadcasting translate result message.
     * Should be used as broadcast receiver filter to get only translate result messages.
     */
    public static final String EVENT_TRANSLATE_RESULT_FILTER = "translate.result";

    /**
     * used as intent filter when broadcasting audio data message.
     * Should be used as broadcast receiver filter to get only audio data messages.
     */
    public static final String EVENT_AUDIO_DATA_FILTER = "audio.data.message";

//    public static final String SAMPLE_RATE_KEY = "sample.rate";

    /**
     * used as the key of a system message error code
     */
    public static final String SYSTEM_MSG_CODE = "system.message.code";

    /**
     * used as the key of a system message description
     */
    public static final String SYSTEM_MSG_DESCRIPTION = "system.message.description";

    /**
     * used as the key of a message data
     */
    public static final String MSG_KEY = "lexifone.broadcast.message";

    /**
     * system message data when getting unexpected response from server
     */
    public static final String RESPONSE_INVALID = "response.invalid";

    /**
     * change language status indicating that change languages request handled successfully
     */
    public static final String CHANGE_SETTING_SUCCESS = "CHANGE_SUCCESS";

    /**
     * status indicating an internal error occurred
     */
    public static final String INTERNAL_ERROR = "application internal error";

    /**
     * status indicating that request timeout expired before getting response from server
     */
    public static final String CHANGE_LANGUAGE_TIMEOUT = "request timeout";

    /**
     * translate result key to results map
     */
    public static final String TEXT_RESULT_KEY = "result";

    /**
     * translate result key to original transcript. A result map might contain
     * multiple translations of the same transcript. Original transcript is the text
     * from which translation results created.
     */
    public static final String NATIVE_TRANSCRIPT_KEY = "native_transcript";


    /**
     * translate result transcript content indicating the server failed to perform speech to text
     */
    public static final String AUDIO_INTERPETER_NOMATCH = "NOMATCH";

    /**
     * translate result key to original language transcript (the language in which the
     * speech detection performed or text was typed and sent for translation)
     */
    public static final String RESULT_ORIGINAL_LANGUAGE = "originalLanguage";

    /**
     * translate result key to speaker id - relevant in conference mode only
     */
    public static final String RESULT_SPEKAER_ID = "speakerId";

    /**
     * translate result key to speaker name - relevant in conference mode only
     */
    public static final String RESULT_SPEKAER_NAME = "speakerDisplayName";

    /**
     * general error code
     */
    public static final String CODE_GENERAL_ERROR = "100";

    /**
     * failed to get media url error code
     */
    public static final String CODE_GET_MEDIA_SERVER_URL_ERROR = "101";

    /**
     * request timeout error code
     */
    public static final String CODE_REQUEST_TIMEOUT_ERROR = "102";

    static final String TEXT_MESSAGE_CODE = "code";
    static final String TEXT_MESSAGE_DESCRIPTION = "description";
    static final String UNSUPPORTED_STATUS = "unsupported message status: %s";
    static final String UNSUPPORTED_MESSAGE_TYPE = "unsupported message type: %s";
    static final String UNSUPPORTED_MESSAGE = "unsupported message: %s";
    static final String CONNECTION_FAILURE = "failed to create connection";
    static final String TEXT_TO_SPEECH_ERROR = "text_to_speech_error";

    static final String USER_VALIDATED = "VALIDATED";
    static final String TEXT_MESSAGE_STATUS = "status";

    static final int MIN_CONNECTION_TIMEOUT_SECONDS = 5;
    static final int MAX_CONNECTION_TIMEOUT_SECONDS = 120;
    static final int DEFAULT_CONNECTION_TIMEOUT_SECONDS = 10;
    static final int DEFAULT_CHANGE_LANGUAGES_TIMEOUT_SECONDS = 5;

    private MediaServerConnector mediaServerConnector = new MediaServerConnector();

    /**
     * client-server connection status possible values
     */
    public enum ConnectionStatus {
        NOT_CONNECTED, CONNECTING, CONNECTED, CONNECTED_AND_VALIDATED, DISCONNECTING;

        public boolean isValidated() {
            return this.ordinal() == CONNECTED_AND_VALIDATED.ordinal();
        }

        public boolean notConnected() {
            return this.ordinal() == NOT_CONNECTED.ordinal();
        }

        public boolean connectionOpen() {
            return this.ordinal() == CONNECTED.ordinal() || this.ordinal() == CONNECTED_AND_VALIDATED.ordinal();
        }
    }

    enum ChangeLanguagesStatus {
        CHANGE_IN_PROCESS, CHANGE_FINISHED_SUCCESS, CHANGE_FINISHED_FAILURE;

        public boolean isInPorcess() {
            return this.ordinal() == CHANGE_IN_PROCESS.ordinal();
        }

        public boolean isFinishSuccess() {
            return this.ordinal() == CHANGE_FINISHED_SUCCESS.ordinal();
        }

        public boolean isFinishFailure() {
            return this.ordinal() == CHANGE_FINISHED_FAILURE.ordinal();
        }
    }

    enum ClinetToServerMessageTypes {
        AUTHENTICATION_AND_AUTHORIZATION, CHANGE_LANGUAGES, STOP, TEXT_TRANSLATION, TEXT_TO_SPEECH
    }

    enum ServerToClientMessageTypes {
        authorizationStatus, changeLanguagesStatus, binaryTTSFile, textTranslationResults, textToSpeechResults;
    }

    static {
        System.loadLibrary("speex"); // Notice lack of lib prefix
    }

    private final static String LANG_INVALID_ERROR = "Language code is not valid: %s";
    private final static String LANG_EMPTY_ERROR = "Languages cannot be empty";
    private final static String SAME_LANG_ERROR_PATTERN = "Dialects are of same language: %s";
    private final static String CHANGE_LANGUAGES_SUCCESS = "Successfully changed languages";

    private final static String USER_NAME_EMPTY_ERROR = "User name cannot be empty";
    private final static String PASSWORD_EMPTY_ERROR = "Password cannot be empty";
    private final static String LEXIFONE_ADDRESS_EMPTY_ERROR = "lexifone address cannot be empty";
    private final static String CUSTOMER_EMPTY_ERROR = "Customer identifier cannot be empty";

    private static LexifoneManager instance = null;
    private static final String TAG = "LexifoneManager";

    private long startConnectionTime = 0;
    //    private long translationTime = 0;
    private boolean isMatch;
    private WebSocketConnection webSocketConnection;
    private LexifoneWebSocketConnectionObserver webSocketConnectionObserver;
    private int retryConnectionCount = 0;
    private boolean gotValidMessage = false;
    private boolean stoppedCalled = false;
    private LexifoneSetting descriptor = new LexifoneSetting();//Snabel::update the media url here...
    LexifoneSettingView lexifoneView;
    ConnectionStatus connectStatus = ConnectionStatus.NOT_CONNECTED;
    private Broadcast broadCast;
    private short[] previousData = null;
    private String changeLangNative;
    private String[] changeLangForeign;
    private boolean createNewConnection = false;
    private List<byte[]> audioToSend = new ArrayList<>();
    private int currentAudioToSendLen = 0, maximumAudioToSendLen = 100000;
    private String validateSettingsMessage;
    private ChangeLanguagesStatus changeLanguagesStatus = ChangeLanguagesStatus.CHANGE_FINISHED_SUCCESS;
    private Timer changeLanguagesTimer = new Timer();
    private TimerTask changeLanguagesTimerTask;
    private SpeexEncoder encoder = null;


    /**
     * private CTOR
     */
    private LexifoneManager() {
    }

    /**
     * get LexifoneManager instance.
     * @return LexifoneManager singleton instance
     */
    public static LexifoneManager getInstance() {
        if (instance == null) synchronized (LexifoneManager.class) {
            if (instance == null) {
                instance = new LexifoneManager();
            }
        }
        return instance;
    }

    /**
     * @return a map of the supported languahe codes ant its display name. The method is deprecated, please use getLanguaeDescriptorMap
     */
    @Deprecated
    public Map<String, String> getLanguaeMap() {
        return Language.getDialectMap();
    }

    /**
     * get a map of languages that are supported by Lexifone services.
     * @param lexifoneServerAddress the lexifone server address to fetch the languages from - if null, the list shall be read from a local copy.
     * @return a map of the language codes and their matching descriptor objects
     */
    public Map<String, LanguageDescriptor> getLanguaeDescriptorMap(String lexifoneServerAddress) {
        if (lexifoneServerAddress == null || Language.isLanguageRefreshed()) {
            return Language.getLanguageDescriptorMap();
        } else {
            try {
                List<Map<String, Object>> languageList = getLanguageInformation(lexifoneServerAddress);
                if (languageList != null) {
                    Language.refreshListOfLanguages(languageList);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return Language.getLanguageDescriptorMap();
        }
    }

    private List<Map<String, Object>> getLanguageInformation(final String serverAddress) {
        ExecutorService executor = Executors.newSingleThreadExecutor();

        String errorMessage = null;
        List<Map<String, Object>> languageList = null;
        try {
            languageList = executor.submit(new Callable<List<Map<String, Object>>>() {
                @Override
                public List<Map<String, Object>> call() throws Exception {
                    return mediaServerConnector.getLanguageInformation(serverAddress);
                }
            }).get(3, TimeUnit.SECONDS);
            if (languageList == null) {
                Log.e(TAG, "getLanguageInformation - failed to get languageList");
                return null;
            }
        } catch (TimeoutException ex) {
            errorMessage = "getLanguageInformation - request time out";
        } catch (InterruptedException e) {
            errorMessage = "getLanguageInformation -  - InterruptedException";
        } catch (Exception e) {
            errorMessage = "getLanguageInformation - Exception: " + e.getLocalizedMessage();
        } finally {
            executor.shutdownNow(); // cleanup
        }
        if (errorMessage != null) {
            Log.e(TAG, errorMessage);
            return null;
        }else {
            return languageList;
        }
    }


    @Deprecated
    /**
     * get the current connection status.
     * @return ConnectionStatus enumerator representing Lexifone Manager connection status.
     * this method should be replaced by direct call to connection status methods
     * like userValidated(), connectionOpen(), etc.
     */
    public ConnectionStatus getConnectStatus() {
        return this.connectStatus;
    }

    /**
     * get the connection opened and user validated flag.
     * @return true if connection to server is open and user validated.
     */
    public boolean userValidated(){
        return connectStatus.isValidated();
    }

    /**
     * get the connection opened flag.
     * @return true if connection to server is open.
     */
    public boolean connectionOpen(){
        return connectStatus.connectionOpen() && webSocketConnection != null && webSocketConnection.isConnected();
    }

    /**
     * get the connection closed flag.
     * @return true if not connected to server.
     */
    public boolean notConnected(){
        return connectStatus.notConnected();
    }

    /**
     * get the current setting native language code.
     * @return the current setting native language code
     */
    public String getNativeLanguage() {
        return descriptor == null ? "" : descriptor.getNativeLanguage();
    }

    /**
     * get the current setting foreign languages codes.
     * @return String[] - languages codes array.
     */
    public String[] getForeignLanguages() {
        return descriptor == null ? new String[0] : descriptor.getForeignLanguages();
    }

    /**
     * get the current setting sample rate.
     * @return integer representing the current setting sample rate
     */
    public int getSampleRate() {
        return descriptor == null ? 0 : descriptor.getSampleRate();
    }

    /**
     * get the current setting do encode audio flag.
     * @return true if current setting is to encode audio data
     */
    public boolean isEncoded() {
        return descriptor == null ? false : descriptor.isEncoded();
    }

    /**
     * get aview of the current setting object.
     * @return LexifoneSettingView, a read only object that contain the current setting.
     */
    public LexifoneSettingView getSetting() {
        if (lexifoneView == null) {
            lexifoneView = new LexifoneSettingView(this.descriptor);
        }
        return lexifoneView;
    }

    /**
     * validate the native and foreign dialect. It is not allowed to use dialects of the same
     * language (e.g. en-us and en-uk)
     *
     * @param nativeDialect  String, the native dialect code
     * @param foreignDialect String, the foreign dialect code
     * @return null if validation passed, other wise the validation error message
     */
    public String validateLanguages(String nativeDialect, String foreignDialect) {
        String validateMessage = null;
        if((validateMessage = validateLanguage(nativeDialect)) != null){
            return validateMessage;
        }
        if((validateMessage = validateLanguage(foreignDialect)) != null){
            return validateMessage;
        }

        LanguageDescriptor nativeLang = Language.getLanguageDescriptorMap().get(nativeDialect);
        LanguageDescriptor foreignLang = Language.getLanguageDescriptorMap().get(foreignDialect);
        if (nativeLang.getLanguageName().equals(foreignLang.getLanguageName())) {
            return String.format(SAME_LANG_ERROR_PATTERN, nativeLang.getLanguageName());
        }
        return null;
    }

    /**
     * validate the language/dialect
     * @param languageCode String, the language code to validate
     * @return null if validation passed, other wise the validation error message
     */
    public String validateLanguage(String languageCode) {
        if(languageCode == null || languageCode.isEmpty()){
            return LANG_EMPTY_ERROR;
        }
        if(!Language.isValidDialect(languageCode)){
            return String.format(LANG_INVALID_ERROR, languageCode);
        }
        return null;
    }

    /**
     * start Lexifone manager to be ready to process application requests.
     * this will create a connection to Lexifone server and authenticate user details.
     * in case authentication failed the connection is closed immediately.
     * if connection created and authentication passed the manager is now ready
     * for application requests. The connection status is passed asynchronously to
     * the application by broadcast a message using LexifoneManager.EVENT_CONNECT_FILTER.
     *
     * @param context         Context, the application context
     * @param lexifoneSetting LexifoneSetting, the session setting as defined by the
     *                        application including user credentials
     * @throws Exception
     */
    public void start(Context context, LexifoneSetting lexifoneSetting) throws Exception {
        start(context, lexifoneSetting, DEFAULT_CONNECTION_TIMEOUT_SECONDS);
    }

    /**
     * start Lexifone manager to be ready to process application requests.
     * this will create a connection to Lexifone server and authenticate user details.
     * in case authentication failed the connection is closed immediately.
     * if connection created and authentication passed the manager is now ready
     * for application requests. The connection status is passed asynchronously to
     * the application by broadcast a message using LexifoneManager.EVENT_CONNECT_FILTER.
     *
     * @param context         Context, the application context
     * @param lexifoneSetting LexifoneSetting, the session setting as defined by the
     *                        application including user credentials
     * @param timeoutSeconds  int, the timeout in seconds to wait for connection. Valid values are
     *                        integer value between 5 to 120.
     * @throws Exception
     */
    public synchronized void start(Context context, LexifoneSetting lexifoneSetting, int timeoutSeconds) throws Exception {
        if (!connectStatus.notConnected()) { // start can be called only if status is NOT_CONNECTED
            if (connectStatus.isValidated()) {
                Log.w(TAG, "start - already connected");
            } else {
                Log.w(TAG, "start - establishing connection");
            }
            return;
        }
        if (!validateSetting(lexifoneSetting)) {
            throw new Exception("Invalid setting - " + validateSettingsMessage);
        }

        Fabric.with(context, new Crashlytics());
        descriptor = lexifoneSetting;
        descriptor.setMessageType(ClinetToServerMessageTypes.AUTHENTICATION_AND_AUTHORIZATION.name());
        broadCast = new Broadcast(context);
        descriptor.setConnectionTimeout(Math.min(Math.max(timeoutSeconds, MIN_CONNECTION_TIMEOUT_SECONDS), MAX_CONNECTION_TIMEOUT_SECONDS));
        Log.i(TAG, "start - running connect thread with timeout: " + descriptor.getConnectionTimeout() + "[sec]");
        new Thread(new Runnable() {
            @Override
            public void run() {
                doConnect();
            }
        }, "connectThread").start();
        updateConnectionStatus(ConnectionStatus.CONNECTING);
    }

    /**
     * stop Lexifone manager from further processing. This will cause disconnection from the server.
     *
     * @throws Exception
     */
    public void stop() throws Exception {
        if (connectStatus.notConnected()) {
            Log.w(TAG, "stop - not connected");
            return;
        }
        sendStopMessage();
    }

    /**
     * translate the given text based on current setting - native and
     * foreign languages. The translation result are passed asynchronously to
     * the application by broadcast using LexifoneManager.EVENT_TRANSLATE_RESULT_FILTER.
     *
     * @param text  String, the text to be translated
     * @throws NoValidConnectionException
     */
    public void translateText(String text) throws Exception {
        Log.d(TAG, "translateText");
        translateText(text, descriptor.getNativeLanguage(), descriptor.getForeignLanguages());
    }

    /**
     * translate the given text based on the given native and foreign languages.
     * The translation result are passed asynchronously to
     * the application by broadcast using LexifoneManager.EVENT_TRANSLATE_RESULT_FILTER.
     *
     * @param text             String, the text to be translated
     * @param nativeLanguage   String, the language in which the text is
     * @param foreignLanguages String, the language to translate to
     * @throws NoValidConnectionException
     */
    public void translateText(String text, String nativeLanguage, String[] foreignLanguages) throws Exception {
        Log.d(TAG, String.format("translateText - text: %s, nativeLanguage: %s, foreignLanguages: %s", text, nativeLanguage, Arrays.toString
                (foreignLanguages)));
        if (isSessionReady() && gotValidMessage) {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("messageType", ClinetToServerMessageTypes.TEXT_TRANSLATION.name());
            map.put("textToTranslate", text);
            map.put("nativeLanguage", nativeLanguage);
            map.put("foreignLanguages", foreignLanguages);
            ObjectMapper objectMapper = new ObjectMapper();
            sendTextMessage(objectMapper.writeValueAsString(map));
        }else{
            throw new NoValidConnectionException("translateText");
        }
    }

    /**
     * change native and foreign dialects for the current setting. The dialects
     * must pass validation. The status of this request is passed asynchronously to
     * the application by broadcast using LexifoneManager.EVENT_CHANGE_LANGS_FILTER.
     *
     * @param nativeDialect   String, the native dialect code
     * @param foreignDialects String, the foreign dialect code
     * @throws NoValidConnectionException
     */
    public synchronized void changeLanguages(String nativeDialect, String[] foreignDialects) throws Exception {
        if (changeLanguagesStatus.isInPorcess()) {
            throw new Exception("change languages already in process");
        }
        removeChangeLanguagesTask();
        String errMessage = validateLanguages(nativeDialect, foreignDialects);
        if (errMessage != null) {
            throw new Exception("Invalid dialects - " + errMessage);
        }
        if (isSessionReady() && gotValidMessage) {
            this.changeLangNative = nativeDialect;
            this.changeLangForeign = foreignDialects;
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("messageType", ClinetToServerMessageTypes.CHANGE_LANGUAGES.name());
            map.put("nativeLanguage", nativeDialect);
            map.put("foreignLanguages", foreignDialects);
            ObjectMapper objectMapper = new ObjectMapper();
            try {
                changeLanguagesStatus = ChangeLanguagesStatus.CHANGE_IN_PROCESS;
                sendTextMessage(objectMapper.writeValueAsString(map));
                createChangeLanguagesTask();
                changeLanguagesTimer.schedule(changeLanguagesTimerTask, DEFAULT_CHANGE_LANGUAGES_TIMEOUT_SECONDS * 1000);
            } catch (Exception e) {
                e.printStackTrace();
                updateChangeLanguagesStatus(INTERNAL_ERROR);
            }
        }else{
            throw new NoValidConnectionException("changeLanguages");
        }
    }

    private void createChangeLanguagesTask() {
        changeLanguagesTimerTask = new TimerTask() {
            @Override
            public void run() {
                updateChangeLanguagesStatus(CHANGE_LANGUAGE_TIMEOUT);
            }
        };
    }

    private void removeChangeLanguagesTask(){
        if(changeLanguagesTimerTask != null) {
            if(changeLanguagesTimerTask.cancel()){
                Log.d(TAG, "changeLanguagesTimerTask successfully cancelled");
            }else{
                Log.d(TAG, "changeLanguagesTimerTask - nothing to cancel");
            }
            changeLanguagesTimerTask = null;
        }
    }

    private synchronized void updateChangeLanguagesStatus(String status){
        if(changeLanguagesStatus.isInPorcess()){
            if (CHANGE_SETTING_SUCCESS.equals(status)) {
                changeLanguagesStatus = ChangeLanguagesStatus.CHANGE_FINISHED_SUCCESS;
                descriptor.setNativeLanguage(changeLangNative);
                descriptor.setForeignLanguages(changeLangForeign);
//                            broadCast.sendSystemMessage(CHANGE_LANGUAGES_SUCCESS);
            } else {
                changeLanguagesStatus = ChangeLanguagesStatus.CHANGE_FINISHED_FAILURE;
            }
            broadCast.sendChangeLanguagesStatus(status);
        }else{
            Log.w(TAG, "updateChangeLanguagesStatus - change language not in process - avoid update");
        }
    }

    /**
     * perform text to speech processing. The audio result is passed asynchronously to
     * the application by broadcast using LexifoneManager.EVENT_AUDIO_DATA_FILTER.
     *
     * @param language String, the language code of the text.
     * @param text     String, the text to speak
     * @throws NoValidConnectionException
     */
    public void textToSpeech(String language, String text) throws Exception {
        Log.e("++++++++++++++", "------->doTextToSpeechMessage");
        if (isSessionReady() && gotValidMessage) {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("messageType", ClinetToServerMessageTypes.TEXT_TO_SPEECH.name());
            map.put("ttsText", text);
            map.put("nativeLanguage", language);
            map.put("encoded", descriptor.isEncoded());

            ObjectMapper objectMapper = new ObjectMapper();
            try {
                sendTextMessage(objectMapper.writeValueAsString(map));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else{
            throw new NoValidConnectionException("textToSpeech");
        }
    }

    /**
     * send audio for recognition. The audio data is sent to the server as is (raw data).
     * the translation result are passed asynchronously to
     * the application by broadcast using LexifoneManager.EVENT_TRANSLATE_RESULT_FILTER.
     *
     * @param audioData byte array, the audio data.
     * @throws NoValidConnectionException
     */
    public void sendAudio(byte[] audioData) throws Exception {
        if (isSessionReady() && gotValidMessage) {
            webSocketConnection.sendBinaryMessage(audioData);
        }else{
            throw new NoValidConnectionException("sendAudio");
        }
    }

    /**
     * send audio encoded for recognition. The audio data is first encoded and then sent to
     * the server. the translation result are passed asynchronously to
     * the application by broadcast using LexifoneManager.EVENT_TRANSLATE_RESULT_FILTER.
     *
     * @param audioData short array, the audio data.
     * @throws NoValidConnectionException
     */
    public void sendAudioEncoded(short[] audioData, int offset, int length) throws Exception {
        if (!isSessionReady() || !gotValidMessage) {
            throw new NoValidConnectionException("sendAudioEncoded");
        }
        if (!audioToSend.isEmpty() && !changeLanguagesStatus.isInPorcess()) {
            if (changeLanguagesStatus.isFinishSuccess() && isSessionReady()) {
                Log.i(TAG, "sendAudioEncoded - sending saved audio");
                for (int i = 0; i < audioToSend.size(); i++) {
                    webSocketConnection.sendBinaryMessage(audioToSend.get(i));
                }
            } else {
                Log.w(TAG, "sendAudioEncoded - avoid sending saved data");
            }
            audioToSend.clear();
            currentAudioToSendLen = 0;
        }

        //SpeexEncoder encoder = new SpeexEncoder(descriptor.getSampleRate() == 8000 ? FrequencyBand.WIDE_BAND : FrequencyBand.WIDE_BAND, 10);
        if(encoder == null){
            encoder = new SpeexEncoder(FrequencyBand.WIDE_BAND, 10);
        }
        short[] effectiveAudioData = null;
        int shortRead = 0;
        if (previousData != null) {
            shortRead = previousData.length + length;
            effectiveAudioData = new short[shortRead];
            System.arraycopy(previousData, 0, effectiveAudioData, 0, previousData.length);
            System.arraycopy(audioData, 0, effectiveAudioData, previousData.length, length);
            previousData = null;
        } else {
            effectiveAudioData = audioData;
            shortRead = length;
        }
        int times = shortRead / encoder.getFrameSize();
        short[] data = new short[encoder.getFrameSize()];
        for (int i = 0; i < times; i++) {
            System.arraycopy(effectiveAudioData, i * data.length, data, 0, data.length);
            byte[] encodedData = encoder.encode(data);
            if (isSessionReady()) {
                if (changeLanguagesStatus.isInPorcess()) {
                    if (currentAudioToSendLen + encodedData.length > maximumAudioToSendLen) {
                        Log.e(TAG, "audioToSend reached its maximum length - abort saving current audio");
                        return;
                    } else {
                        byte[] saveData = new byte[encodedData.length];
                        System.arraycopy(encodedData, 0, saveData, 0, encodedData.length);
                        currentAudioToSendLen += encodedData.length;
                        audioToSend.add(saveData);
                    }
                } else {
                    webSocketConnection.sendBinaryMessage(encodedData);
                }
            }
        }
        int remainingByte = shortRead % encoder.getFrameSize();
        if (remainingByte > 0) {
            previousData = new short[remainingByte];
            System.arraycopy(effectiveAudioData, times * encoder.getFrameSize(), previousData, 0, remainingByte);
        }
    }


    private boolean validateSetting(LexifoneSetting lexifoneSetting) {
        validateSettingsMessage = null;
        if (lexifoneSetting.getLexifoneAddress() == null || lexifoneSetting.getLexifoneAddress().isEmpty()) {
            validateSettingsMessage = LEXIFONE_ADDRESS_EMPTY_ERROR;
            return false;
        }
        if (lexifoneSetting.getUserName() == null || lexifoneSetting.getUserName().isEmpty()) {
            validateSettingsMessage = USER_NAME_EMPTY_ERROR;
            return false;
        }
        if (lexifoneSetting.getPassword() == null || lexifoneSetting.getPassword().isEmpty()) {
            validateSettingsMessage = PASSWORD_EMPTY_ERROR;
            return false;
        }
//        if (lexifoneSetting.getCustomerId() == null || lexifoneSetting.getCustomerId().isEmpty()) {
//            validateSettingsMessage = CUSTOMER_EMPTY_ERROR;
//            return false;
//        }
        validateSettingsMessage = validateLanguages(lexifoneSetting.getNativeLanguage(), lexifoneSetting.getForeignLanguages());
        return validateSettingsMessage == null;
    }

    private String validateLanguages(String nativeLanguage, String[] foreignLanguages) {
        if (foreignLanguages == null || foreignLanguages.length == 0) {
            return validateLanguage(nativeLanguage);
        }
        return validateLanguages(nativeLanguage, foreignLanguages[0]);
    }

    private void doConnect() {
        int timeoutSeconds = descriptor.getConnectionTimeout();
        try {
            descriptor.setMediaServerAddress(null);
            String errorMessage = null;

            // first step - try to get the server url
            ExecutorService executor = Executors.newSingleThreadExecutor();

            try {
                String mediaUrl = executor.submit(new Callable<String>() {
                    @Override
                    public String call() throws Exception {
                        return mediaServerConnector.getMediaServerUrl(descriptor);
                    }
                }).get(timeoutSeconds, TimeUnit.SECONDS);
                if (mediaUrl == null || mediaUrl.isEmpty()) {
                    Log.e(TAG, "getMediaUrl - failed to get media url");
                    connectionError(CODE_GET_MEDIA_SERVER_URL_ERROR, "no url returned");
                    return;
                }
                createNewConnection = !mediaUrl.equals(descriptor.getMediaServerAddress());
                descriptor.setMediaServerAddress(mediaUrl);
            } catch (TimeoutException ex) {
                errorMessage = "request time out";
            } catch (InterruptedException e) {
                Log.e(TAG, "executeTask - exception: " + e.getLocalizedMessage());
                errorMessage = "connection error - InterruptedException";
            } catch (Exception e) {
                Log.e(TAG, "executeTask - exception: " + e.getLocalizedMessage());
                errorMessage = "connection error - ExecutionException";
            } finally {
                executor.shutdownNow(); // cleanup
            }
            if (errorMessage != null) {
                connectionError(CODE_GET_MEDIA_SERVER_URL_ERROR, errorMessage);
                return;
            }

            // now try to establish the connection
            executor = Executors.newSingleThreadExecutor();
            Future<?> future = executor.submit(new Runnable() {
                @Override
                public void run() {
                    try {
                        connectToWs();
                    } catch (Exception e) {
                        Log.e(TAG, "doConnect - connectToWs throw exception: " + e.getLocalizedMessage());
                        connectionError(e.getLocalizedMessage());
                    }
                }
            }, timeoutSeconds);

            try {
                future.get(timeoutSeconds, TimeUnit.SECONDS); // wait for task to complete
            } catch (TimeoutException ex) {
                errorMessage = "connection to server time out";
            } catch (InterruptedException e) {
                Log.e(TAG, "executeTask - exception: " + e.getLocalizedMessage());
                errorMessage = "connection error - InterruptedException";
            } catch (ExecutionException e) {
                Log.e(TAG, "executeTask - exception: " + e.getLocalizedMessage());
                errorMessage = "connection error - ExecutionException";
            } finally {
                executor.shutdownNow(); // cleanup
            }
            if (errorMessage != null) {
                invalidateConnection(errorMessage);
            }
        } catch (Exception e) {
            Log.e(TAG, "doConnect - exception: " + e.getLocalizedMessage());
            connectionError(e.getLocalizedMessage());
        }
    }

    private void connectToWs() {
        Log.i(TAG, "connectToWs - start");
        gotValidMessage = false;
        try {
            retryConnectionCount++;
            if (webSocketConnection == null || createNewConnection || retryConnectionCount > 3) {
                createConnection();
                createNewConnection = false;
            } else {
                startConnectionTime = System.currentTimeMillis();
                Log.e(TAG, "connectToWs - begin time :" + startConnectionTime);
                webSocketConnection.reconnect();
            }

        } catch (Exception e) {
            Log.e(TAG, "connectToWs - exception: " + e.getLocalizedMessage());
            connectionError(e.getLocalizedMessage());
        }
    }

    private void connectionError(String message) {
        connectionError(CODE_GENERAL_ERROR, message);
    }

    private void connectionError(String errorCode, String message) {
        broadCast.sendSystemMessage("Error while connecting: " + message);
        updateConnectionStatus(ConnectionStatus.NOT_CONNECTED);
        broadCast.sendSystemMessage(CONNECTION_FAILURE, errorCode, message);
    }

    // in case of connection timeout - mark the connection as invalid to ignore connection messages
    private synchronized void invalidateConnection(String invalidateMessge) {
        if (connectStatus.connectionOpen()) {
            return;
        }
        if (webSocketConnectionObserver != null) {
            webSocketConnectionObserver.valid = false;
        }
        if (webSocketConnection != null) {
            webSocketConnection.disconnect();
        }
        createNewConnection = true;
        connectionError(invalidateMessge);
    }

    private synchronized boolean setConnectionOpened(LexifoneWebSocketConnectionObserver observer) {
        if (observer.valid) { // make sure the observer was not marked as invalid due to connection timeout
            updateConnectionStatus(ConnectionStatus.CONNECTED);
            return true;
        } else {
            return false;
        }
    }

    private void updateConnectionStatus(ConnectionStatus connectionStatus) {
        connectStatus = connectionStatus;
        broadCast.sendConnectStatus(connectStatus.name());
    }

    private void createConnection() throws Exception {
        webSocketConnectionObserver = new LexifoneWebSocketConnectionObserver();
        this.webSocketConnection = new WebSocketConnection();
        WebSocketOptions options = new WebSocketOptions();
        options.setMaxMessagePayloadSize(1024 * 1024);
        options.setMaxFramePayloadSize(1024 * 1024);
        options.setSocketConnectTimeout(1000);
        startConnectionTime = System.currentTimeMillis();
        Log.e(TAG, "createConnection begin time :" + startConnectionTime);
        Log.e(TAG, "createConnection url :" + descriptor.getMediaServerAddress());
        this.webSocketConnection.connect(new URI(descriptor.getMediaServerAddress()), webSocketConnectionObserver, options);
    }

    private synchronized void sendTextMessage(String message) {
        if (isSessionReady()) {
            this.webSocketConnection.sendTextMessage(message);
        }
    }

    private boolean isSessionReady() {
        return this.webSocketConnection != null && this.webSocketConnection.isConnected();
    }

    private byte[] ShortToByte_Twiddle_Method(short[] input) {
        int short_index, byte_index;
        int iterations = input.length;

        byte[] buffer = new byte[input.length * 2];

        short_index = byte_index = 0;

        for (/* NOP */; short_index != iterations; /* NOP */) {
            buffer[byte_index] = (byte) (input[short_index] & 0x00FF);
            buffer[byte_index + 1] = (byte) ((input[short_index] & 0xFF00) >> 8);

            ++short_index;
            byte_index += 2;
        }

        return buffer;
    }

    // Stops the session.
    private synchronized void sendStopMessage() throws Exception {
        if (!stoppedCalled) {
            stoppedCalled = true;
            Log.d(TAG, "Calling STOP message");
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("messageType", ClinetToServerMessageTypes.STOP.name());
            ObjectMapper objectMapper = new ObjectMapper();
            Log.d(TAG, objectMapper.writeValueAsString(map));
            if (isSessionReady()) {
                updateConnectionStatus(ConnectionStatus.DISCONNECTING);
                this.webSocketConnection.sendTextMessage(objectMapper.writeValueAsString(map));
                this.webSocketConnection.disconnect();
            } else {
                updateConnectionStatus(ConnectionStatus.NOT_CONNECTED);
            }
            waitForDisconnect();
        }
    }

    private void waitForDisconnect() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                int times = 0;
                while (times < descriptor.getConnectionTimeout()) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        Log.e(TAG, "waitForDisconnect - InterruptedException");
                    }
                    if (connectStatus != ConnectionStatus.DISCONNECTING) {
                        return;
                    }
                    times++;
                }
                if (connectStatus == ConnectionStatus.DISCONNECTING) {
                    Log.w(TAG, "waitForDisconnect - not disconnected after " + descriptor.getConnectionTimeout() + "[sec] - send stop message");
                    try {
                        if (isSessionReady()) {
                            webSocketConnection.disconnect();
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "waitForDisconnect - disconnect throw exception: " + e.getLocalizedMessage());
                    } finally {
                        updateConnectionStatus(ConnectionStatus.NOT_CONNECTED);
                    }
                }
            }
        }).start();
    }

    private void userAuthenticate() {
        if (isSessionReady() && !gotValidMessage) {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("userName", descriptor.getUserName());
            map.put("password", descriptor.getPassword());
            map.put("sampleRate", descriptor.getSampleRate());
            map.put("nativeLanguage", descriptor.getNativeLanguage());
            map.put("foreignLanguages", descriptor.getForeignLanguages());
            map.put("encoded", descriptor.isEncoded());
            if (descriptor.getSpeakerId() != null) {
                map.put("speakerId", descriptor.getSpeakerId());
            }
            if (descriptor.getGlobalSessionId() != null) {
                map.put("globalSessionId", descriptor.getGlobalSessionId());
            }
            if (descriptor.getDisplayName() != null) {
                map.put("displayName", descriptor.getDisplayName());
            }
            map.put("messageType", ClinetToServerMessageTypes.AUTHENTICATION_AND_AUTHORIZATION.name());
            ObjectMapper objectMapper = new ObjectMapper();
            try {
                sendTextMessage(objectMapper.writeValueAsString(map));
                waitForAuthentication();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void waitForAuthentication() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                int times = 0;
                while (times < descriptor.getConnectionTimeout()) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        Log.e(TAG, "waitForAuthentication - InterruptedException");
                    }
                    if (connectStatus != ConnectionStatus.CONNECTED) {
                        return;
                    }
                    times++;
                }
                if (connectStatus == ConnectionStatus.CONNECTED) {
                    Log.w(TAG, "waitForAuthentication - user not authenticated after " + descriptor.getConnectionTimeout() + "[sec] - send stop message");
                    try {
                        broadCast.sendSystemMessage("did not receive Authentication response in " + descriptor.getConnectionTimeout() + " seconds - abort");
                        sendStopMessage();
                    } catch (Exception e) {
                        Log.e(TAG, "waitForAuthentication - sendStopMessage throw exception: " + e.getLocalizedMessage());
                    }
                }
            }
        }).start();
    }

    class LexifoneWebSocketConnectionObserver implements WebSocket.WebSocketConnectionObserver {
        boolean valid = true;
        boolean closed = false;

        @Override
        public void onOpen() {
            closed = false;
            if (setConnectionOpened(this)) {
                stoppedCalled = false;
                retryConnectionCount = 0;
                System.out.println("Connected!!!!");
                Log.i(TAG, "onOpen - create connection time :" + (System.currentTimeMillis() - startConnectionTime));
                userAuthenticate();
            } else {
                Log.w(TAG, "onOpen - observer not valid - ignore");
                return;
            }
        }

        @Override
        public void onClose(WebSocketCloseNotification code, String reason) {
            if (!valid) {
                Log.w(TAG, "onClose called on invalid observer");
            }
            if(closed){
                Log.w(TAG, "onClose called more than once");
            }else {
                Log.i(TAG, "onClose");
                stoppedCalled = true;
                if (reason != null) {
                    Log.e(TAG, "close connection reason:" + reason);
                    if (connectStatus == ConnectionStatus.CONNECTING || connectStatus == ConnectionStatus.CONNECTED_AND_VALIDATED) {
                        broadCast.sendSystemMessage("disconnect reason: " + reason);
                    }
                }
                updateConnectionStatus(ConnectionStatus.NOT_CONNECTED);
                closed = true;
            }
        }

        @Override
        public void onTextMessage(String payload) {
            Log.e(TAG, String.format("Got string message! %s", payload));
            try {
                ObjectMapper objectMapper = new ObjectMapper();
                @SuppressWarnings("unchecked")
                Map<String, String> map = objectMapper.readValue(payload, HashMap.class);
                String messageType = map.get("messageType");
                String status = map.get(TEXT_MESSAGE_STATUS);
                String description = map.get(TEXT_MESSAGE_DESCRIPTION);
                // makes sure that the first message from server includes a verified status.
                // In case of a verified status, starts to capture audio from the microphone
                // In case of none verified status stops the session
                if (messageType == null) {
                    broadCast.sendSystemMessage(RESPONSE_INVALID, CODE_GENERAL_ERROR, "message type empty");
                } else if (!gotValidMessage) {
                    if (messageType.equals(ServerToClientMessageTypes.authorizationStatus.name())) {
                        if (status.equals(USER_VALIDATED)) {
                            gotValidMessage = true;
                            Log.d(TAG, "User is validated");
                            updateConnectionStatus(ConnectionStatus.CONNECTED_AND_VALIDATED);
//                            broadCast.sendSystemMessage("Connected and validated successfully");
                        } else if (!status.equals(USER_VALIDATED)) {
                            Log.d(TAG, "User is not validated - aborting");
                            broadCast.sendSystemMessage("Session not valid - disconnecting", map.get(TEXT_MESSAGE_CODE), status + (description == null ? "" : ": " + description));
                            sendStopMessage();
                        } else {
                            broadCast.sendSystemMessage(RESPONSE_INVALID, CODE_GENERAL_ERROR, String.format(UNSUPPORTED_STATUS, status));
                        }
                    }
                } else {
                    if (messageType.equals(ServerToClientMessageTypes.textTranslationResults.name())) {
                        String origLanguage = map.get(RESULT_ORIGINAL_LANGUAGE);
                        if(origLanguage == null || origLanguage.isEmpty()){
                            origLanguage = descriptor.getNativeLanguage();
                        }
                        String nativeString = map.get(origLanguage);
                        Log.e(TAG, "nativeString:" + nativeString);
                        isMatch = !AUDIO_INTERPETER_NOMATCH.equals(nativeString);
                        Log.e(TAG, "----->isMatch:" + isMatch);
                        Map<String, String> result = new HashMap<String, String>();
                        for (String key : map.keySet()) {
                            String val = map.get(key);
                            result.put(key, val);
                        }
                        Bundle bundle = new Bundle();
                        bundle.putSerializable(TEXT_RESULT_KEY, (Serializable) result);
                        broadCast.sendTranslateResultMsg(nativeString, bundle);
                    } else if (messageType.equals(ServerToClientMessageTypes.changeLanguagesStatus.name())) {
                        removeChangeLanguagesTask();
                        updateChangeLanguagesStatus(status);
                        if (!CHANGE_SETTING_SUCCESS.equals(status)) {
                            broadCast.sendSystemMessage("Failed to change languages", status, description);
                        }
                    } else if (messageType.equals(ServerToClientMessageTypes.textToSpeechResults.name())) {
                        if (TEXT_TO_SPEECH_ERROR.equals(status)) {
                            broadCast.sendSystemMessage("Failed to perform text to speech", status, description);
                        }
                    }else if (messageType.equals(ServerToClientMessageTypes.binaryTTSFile.name())) {
                        // nothing to do currently
                    } else {
                        broadCast.sendSystemMessage(RESPONSE_INVALID, CODE_GENERAL_ERROR, String.format(UNSUPPORTED_MESSAGE_TYPE, messageType));
                    }
                }
            } catch (Exception e) {
                Log.e(TAG, e.getMessage(), e);
                broadCast.sendSystemMessage(RESPONSE_INVALID, CODE_GENERAL_ERROR, e.getLocalizedMessage());
            }
        }

        @Override
        public void onRawTextMessage(byte[] payload) {
            broadCast.sendSystemMessage(RESPONSE_INVALID, CODE_GENERAL_ERROR, String.format(UNSUPPORTED_MESSAGE, "RawTextMessage"));
        }

        @Override
        public void onBinaryMessage(byte[] payload) {
            Log.d(TAG, "payload size:" + payload.length);
            if (isMatch && payload.length > 44) {
                try {
                    Bundle bundle = new Bundle();
                    if (descriptor.isEncoded()) {
                        SpeexDecoder decoder = new SpeexDecoder(FrequencyBand.WIDE_BAND);
                        int decodedLen = 0;
                        ByteArrayOutputStream out = new ByteArrayOutputStream();
                        while (decodedLen < payload.length) {
                            byte[] encodedAudio = new byte[42];
                            System.arraycopy(payload, decodedLen, encodedAudio, 0, Math.min(encodedAudio.length, payload.length - decodedLen));
                            decodedLen += encodedAudio.length;
                            short[] decoded = decoder.decode(encodedAudio);
                            byte[] decodedBytes = ShortToByte_Twiddle_Method(decoded);
                            out.write(decodedBytes);
                        }
                        broadCast.sendAudioData(out.toByteArray());
                    } else {
                        byte[] pcmData = new byte[payload.length - 44];
                        System.arraycopy(payload, 44, pcmData, 0, pcmData.length);
                        broadCast.sendAudioData(pcmData);
                    }
                } catch (Exception e) {
                    Log.e(TAG, "onBinaryMessage - exception: " + e.getLocalizedMessage());
                }
            }
        }
    }
}