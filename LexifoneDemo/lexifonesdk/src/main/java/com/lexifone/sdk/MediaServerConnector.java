package com.lexifone.sdk;

import android.os.Build;
import android.os.StrictMode;
import android.util.Log;

import org.codehaus.jackson.map.ObjectMapper;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.net.URL;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;


/**
 * Created by snabel on 6/16/2015.
 */
public class MediaServerConnector {
    private static final String TAG = "MediaServerConnector";

    private static final String UTF_8 = "UTF-8";
    private static final String CONTENT_TYPE_HEADER = "Content-type";
    private static final String APPLICATION_JSON_TYPE = "application/json";
    private static final String ACCEPT_HEADER = "Accept";
    private static final String CHARSET_HEADER = "Charset";
    private static final String SEPARATOR = ":";
    private static final int DEFAULT_SSL_PORT = 443;
    private static final String LEXIFONE_MAIN_URL_PATTERN = "https://%s/lexifone-rt/dynamic/service/routing/getMediaUrl";
    private static final String LEXIFONE_GET_SUPPORTED_LANGUAGES_PATTERN = "https://%s/lexifone-rt/dynamic/service/sdk/getSupportedLanguages";

    public String getMediaServerUrl(LexifoneSetting lexifoneSetting) {
        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            String lexifoneAdress = lexifoneSetting.getLexifoneAddress();

            String lexifoneMainUrl = String.format(LEXIFONE_MAIN_URL_PATTERN, lexifoneAdress);
            HttpsURLConnection urlConnection = getNewHttpClient(lexifoneMainUrl);
            urlConnection.setRequestMethod("POST");
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            urlConnection.setRequestProperty(CONTENT_TYPE_HEADER, APPLICATION_JSON_TYPE);
            //HttpClient client = new DefaultHttpClient();

            ObjectMapper objectMapper = new ObjectMapper();
            StringWriter writer = new StringWriter();

            Map<String, String> paramsMap = new HashMap<String, String>();
            paramsMap.put("latitude", Double.toString(lexifoneSetting.getLatitude()));
            paramsMap.put("longitude", Double.toString(lexifoneSetting.getLongitude()));
            paramsMap.put("customerId", lexifoneSetting.getCustomerId());
            paramsMap.put("userId", lexifoneSetting.getUserName());
            paramsMap.put("resourceType", "API");
            paramsMap.put("allowProxy", "true");
            objectMapper.writeValue(writer, paramsMap);
            OutputStream os = urlConnection.getOutputStream();
            BufferedWriter bwriter = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            bwriter.write(writer.toString());
            bwriter.flush();
            bwriter.close();
            os.close();
            urlConnection.connect();
            int responseCode = urlConnection.getResponseCode();
            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String mediaUrl = readStream(urlConnection.getInputStream());
                return mediaUrl;
            } else {
                throw new IllegalStateException("Got wrong response code:" + responseCode);
            }
        } catch (Exception e) {
            Log.e(TAG, "getMediaUrl - exception: " + e.getLocalizedMessage());
        }
        return null;
    }

    private String readStream(InputStream in) {
        BufferedReader reader = null;
        StringBuffer response = new StringBuffer();
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return response.toString();
    }


    public List<Map<String, Object>> getLanguageInformation(String serverAddress) {
        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            String lexigoneGetSupportedLanguage = String.format(LEXIFONE_GET_SUPPORTED_LANGUAGES_PATTERN, serverAddress);
            HttpsURLConnection urlConnection = getNewHttpClient(lexigoneGetSupportedLanguage);
            int responseCode = urlConnection.getResponseCode();
            if (responseCode == HttpsURLConnection.HTTP_OK) {
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                ObjectMapper objectMapper = new ObjectMapper();
                return objectMapper.readValue(in, List.class);
            }

        } catch (Exception e) {
            Log.e(TAG, "getLanguageInformation - exception: " + e.getLocalizedMessage());
        }
        return null;
    }



    public HttpsURLConnection getNewHttpClient(String urlString) throws Exception {
        X509TrustManager easyTrustManager = new X509TrustManager() {
            public void checkClientTrusted(
                    X509Certificate[] chain,
                    String authType) throws CertificateException {
            }

            public void checkServerTrusted(
                    X509Certificate[] chain,
                    String authType) throws CertificateException {
            }

            public X509Certificate[] getAcceptedIssuers() {
                return null;
            }

        };
        HostnameVerifier hostnameVerifier = new HostnameVerifier() {
            @Override
            public boolean verify(String hostname, SSLSession session) {
                HostnameVerifier hv =
                        HttpsURLConnection.getDefaultHostnameVerifier();
                return true;
            }
        };

        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts = new TrustManager[]{easyTrustManager};
        SSLContext context = SSLContext.getInstance("TLS");
        context.init(null, trustAllCerts, new java.security.SecureRandom());
        HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
        URL url = new URL(urlString);
        HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();
        urlConnection.setSSLSocketFactory(context.getSocketFactory());
        urlConnection.setHostnameVerifier(hostnameVerifier);
        urlConnection.setConnectTimeout(10*1000);
        urlConnection.setRequestProperty(ACCEPT_HEADER, APPLICATION_JSON_TYPE);
        urlConnection.setRequestProperty(CHARSET_HEADER, UTF_8);
        if (Integer.parseInt(Build.VERSION.SDK) < Build.VERSION_CODES.FROYO) {
            System.setProperty("http.keepAlive", "false");

        }
        return urlConnection;
    }
}