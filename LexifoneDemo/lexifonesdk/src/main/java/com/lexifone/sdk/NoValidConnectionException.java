package com.lexifone.sdk;

/**
 * thrown when there is attempt to perform an operation involving the server
 * while there is no open connection or user is not validated.
 */
public class NoValidConnectionException extends Exception {

    private static final String MESSAGE = "connection state is not valid for request processing";

    public NoValidConnectionException() {
        super(MESSAGE);
    }

    public NoValidConnectionException(String methodName) {
        super(methodName + " - " + MESSAGE);
    }
}
